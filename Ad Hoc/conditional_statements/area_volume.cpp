/*
    write a program that ask the user to select an option for a menu.
    The menu will have the following options, area of tirangle, area
    of rectangle, area of circle, volume of cylinder, volume of sphere
    and volume of a cube.
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int option;

    double radius, height, length, width, base, edge;

    cout<<"Enter Option"<<endl;
    cout<<"1. Area of Triangle"<<endl;
    cout<<"2. Area of Rectangle"<<endl;
    cout<<"3. Area of Circle"<<endl;
    cout<<"4. Volume of Cylinder"<<endl;
    cout<<"5. Volume of Sphere"<<endl;
    cout<<"6. Volume of Cube"<<endl;

    cin>>option;

    switch(option){
        case 1:

            cout<<"Enter Base"<<endl;
            cin>>base;

            cout<<"Enter Height"<<endl;
            cin>>height;

            cout<<"Area of Triangle, Base - "<<base<<", Height - "<<height<<": "<<.5*base*height<<endl;

            break;

        case 2:

            cout<<"Enter Length"<<endl;
            cin>>length;

            cout<<"Enter Width"<<endl;
            cin>>width;

            cout<<"Area of Rectangle, Length - "<<length<<", Width - "<<width<<": "<<length*width<<endl;

            break;

        case 3:

            cout<<"Enter Radius"<<endl;
            cin>>radius;

            cout<<"Area of Circle, Radius - "<<radius<<": "<<M_PI*pow(radius,2)<<endl;

            break;

        case 4:
            cout<<"Enter Radius"<<endl;
            cin>>radius;

            cout<<"Enter Height"<<endl;
            cin>>height;

            cout<<"Volume of Cylinder, Radius - "<<radius<<", Height - "<<height<<": "<<M_PI*pow(radius,2)*height<<endl;

            break;

        case 5:
            cout<<"Enter Radius"<<endl;
            cin>>radius;

            cout<<"Volume of Sphere, Radius - "<<radius<<": "<<(4/3.0)*M_PI*pow(radius,3)<<endl;

            break;

        case 6:

            cout<<"Enter Edge"<<endl;
            cin>>edge;

            cout<<"Volume of Cube, Edge - "<<edge<<": "<<pow(edge,3)<<endl;

            break;

    }
    return 0;
}
