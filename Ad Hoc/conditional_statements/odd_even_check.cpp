/*
    take a number from the user and check if it is odd (not divisible by 2) or even (divisible by 2).

*/

#include <iostream>

using namespace std;

int main(){

    int number;

    cout<<"Enter a Number"<<endl;
    cin>>number;

    if(number % 2 == 0){
        cout<<number<<" is even."<<endl;
    }else{
        cout<<number<<" is odd."<<endl;
    }

    return 0;
}
