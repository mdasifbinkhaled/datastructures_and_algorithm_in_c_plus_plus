/*
    write a program that takes a character from the user and check if it is a
    lower case letter, an upper case letter, a digit or a symbol
*/

#include <iostream>
using namespace std;

int main()
{

    char character;

    cout<<"Enter a Character"<<endl;

    cin>>character;


    if(character>=48 && character<=57)
    {
        cout<<character<<" is a Digit"<<endl;
    }
    else if((character>=33 && character<=47) || (character>=58 && character<=54)|| (character>=91 && character<=96)|| (character>=123 && character<=126))
    {
        cout<<character<<" is a Symbol"<<endl;
    }
    else if(character>=65 && character<=90)
    {
        cout<<character<<" is an Upper Case Letter"<<endl;
    }
    else if(character>=97 && character<=122)
    {
        cout<<character<<" is a Lower Case Letter"<<endl;
    }


    return 0;
}
