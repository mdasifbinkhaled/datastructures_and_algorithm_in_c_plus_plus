/*
    take a number from the user and check whether it is divisible by both a and b.

*/

#include <iostream>

using namespace std;

int main(){

    int a,b, number;

    cout<<"Enter the First Divisor"<<endl;
    cin>>a;

    cout<<"Enter the Second Divisor"<<endl;
    cin>>b;

    cout<<"Enter a Number"<<endl;
    cin>>number;

    if(number%a==0 && number%b==0){
        cout<<number<<" is divisible by both "<<a<<" and "<<b<<endl;
    }else{
        cout<<number<<" is not divisible by both "<<a<<" and "<<b<<endl;
    }

    return 0;
}
