/*
    take a number from the user and check whether it is positive or negative.

*/

#include <iostream>

using namespace std;

int main(){

    double number;

    cout<<"Enter a Number"<<endl;
    cin>>number;

    if(number >= 0){
        cout<<number<<" is positive."<<endl;
    }else{
        cout<<number<<" is negative."<<endl;
    }

    return 0;
}
