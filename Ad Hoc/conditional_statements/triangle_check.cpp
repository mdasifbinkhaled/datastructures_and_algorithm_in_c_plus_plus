/*
    write a program that takes the lengths of the three sides of a
    triangle from the user and cheks if the sides form a right triangle
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double x, y, z,x_square,y_square,z_square;

    cout<<"Enter the First Side"<<endl;
    cin>>x;

    cout<<"Enter the Second Side"<<endl;
    cin>>y;

    cout<<"Enter the Third Side"<<endl;
    cin>>z;

    x_square=pow(x,2);
    y_square=pow(y,2);
    z_square=pow(z,2);

    if((x_square+y_square==z_square)||(x_square+z_square==y_square)||(y_square+z_square==x_square))
    {
        cout<<"Right Triangle"<<endl;
    }
    else
    {
        cout<<"Not a Right Triangle"<<endl;
    }


    return 0;
}
