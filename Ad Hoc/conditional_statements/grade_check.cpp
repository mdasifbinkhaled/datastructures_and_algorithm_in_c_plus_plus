/*
    write a program that implements the following grading system

    [85, 100] A
    [80, 85)  A-
    [75, 80)  B+
    [70, 75)  B
    [65, 70)  B-
    [60, 65)  C+
    [55, 60)  C
    [50, 55)  C-
    [45, 50)  D+
    [40, 45)  D
    [0, 40)   F

*/

#include <iostream>

using namespace std;

int main(){

    int marks;
    cout<<"Enter Your Marks"<<endl;

    cin>>marks;

    if(marks>=85 && marks< 100){
        cout<<"Grade: A"<<endl;
    }else if(marks>=80 && marks< 85){
        cout<<"Grade: A-"<<endl;
    }else if(marks>=75 && marks< 80){
        cout<<"Grade: B+"<<endl;
    }else if(marks>=70 && marks< 75){
        cout<<"Grade: B"<<endl;
    }else if(marks>=65 && marks< 70){
        cout<<"Grade: B-"<<endl;
    }else if(marks>=60 && marks< 65){
        cout<<"Grade: C+"<<endl;
    }else if(marks>=55 && marks< 60){
        cout<<"Grade: C"<<endl;
    }else if(marks>=50 && marks< 55){
        cout<<"Grade: C-"<<endl;
    }else if(marks>=45 && marks< 50){
        cout<<"Grade: D+"<<endl;
    }else if(marks>=40 && marks< 45){
        cout<<"Grade: D"<<endl;
    }else if(marks>=0 && marks< 40){
        cout<<"Grade: F"<<endl;
    }


    return 0;
}
