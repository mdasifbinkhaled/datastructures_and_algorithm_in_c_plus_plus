/*
    write a program to compute the roots of a quadratic equation
    ax^2 + bx + c = 0 for finding the root
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double a, b, c,b2_4ac;

    cout<<"Enter the First Number"<<endl;
    cin>>a;

    cout<<"Enter the Second Number"<<endl;
    cin>>b;

    cout<<"Enter the Third Number"<<endl;
    cin>>c;

    b2_4ac = pow(b,2)-4*a*c;

    if(b2_4ac>0)
    {
        cout<< "x = "<<((-b+sqrt(b2_4ac))/2*a) <<endl;
        cout<< "x = "<<((-b-sqrt(b2_4ac))/2*a) <<endl;
    }
    else
    {
        cout<< "x = "<<-b/2*a<<" + "<<b2_4ac/2*a<<"i"<<endl;
        cout<< "x = "<<-b/2*a<<" - "<<b2_4ac/2*a<<"i"<<endl;
    }

    return 0;
}
