/*
    write a program that take the user;s age as input and the finda out what stage of
    human life they are in using the table below.

    [0, 3)    Infancy
    [3, 12)   Childhood
    [12, 20)  Adolescence
    [20, 40)  Young Adulthood
    [40, 65)  Mature Adulthood
    [65, --)  Late Adulthood

*/

#include <iostream>

using namespace std;

int main(){

    int age;
    cout<<"Enter Your Age"<<endl;

    cin>>age;

    if(age>=0 && age< 3){
        cout<<"Stage: Infancy"<<endl;
    }else if(age>=3 && age< 12){
        cout<<"Stage: Childhood"<<endl;
    }else if(age>=12 && age< 20){
        cout<<"Stage: Adolescence"<<endl;
    }else if(age>=20 && age< 40){
        cout<<"Stage: Young Adulthood"<<endl;
    }else if(age>=40 && age< 65){
        cout<<"Stage: Mature Adulthood"<<endl;
    }else if(age>=65){
        cout<<"Stage: Late Adulthood"<<endl;
    }


    return 0;
}
