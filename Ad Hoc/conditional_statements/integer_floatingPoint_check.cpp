/*
    take a number from the user and check whether it is integer or floating point number.

*/

#include <iostream>

using namespace std;

int main(){

    double number;

    cout<<"Enter a Number"<<endl;
    cin>>number;

    if(int(number)==number){
        cout<<number<<" is a integer number."<<endl;
    }else{
        cout<<number<<" is a floating point number."<<endl;
    }

    return 0;
}
