    /*
        write a program that checks if a number is within the range (x,y) and divisible
        by a or within the range (p,q) and divisible by b

    */

    #include <iostream>

    using namespace std;

    int main(){

        int x,y,p,q,a,b, number;

        cout<<"Enter the Lower Bound"<<endl;
        cin>>x;

        cout<<"Enter the Upper Bound"<<endl;
        cin>>y;

        cout<<"Enter the Lower Bound"<<endl;
        cin>>p;

        cout<<"Enter the Upper Bound"<<endl;
        cin>>q;

        cout<<"Enter the First Divisor"<<endl;
        cin>>a;

        cout<<"Enter the Second Divisor"<<endl;
        cin>>b;

        cout<<"Enter a Number"<<endl;
        cin>>number;

        if(number>x && number<y){

            cout<<number<<" is greater than "<<x<<" and less than "<<y;

            if(number%a==0){
                cout<<" and it is divisible "<<a<<endl;
            }else{
                cout<<" and it is not divisible "<<a<<endl;
            }
        }else if(number>p && number<q){

            cout<<number<<" is greater than "<<p<<" and less than "<<q;


            if(number%b==0){
                cout<<" and it is divisible "<<b<<endl;
            }else{
                cout<<" and it is not divisible "<<b<<endl;
            }
        }else{

            cout<<number<<" is not greater than "<<x<<" and less than "<<y<<" and divisible by "<<a<<
            " or it is not greater than "<<p<<" and less than "<<q<<" and it is not divisible by "<<b<<endl;
        }

        return 0;
    }
