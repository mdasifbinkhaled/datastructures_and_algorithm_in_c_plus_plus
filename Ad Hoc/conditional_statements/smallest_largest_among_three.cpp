/*
    write a program that take three numbers from the user and finds out
    the largest and the smallest number.
*/

#include <iostream>
using namespace std;


int main()
{

    double first_number, second_number, third_number;

    cout<<"Enter the First Number"<<endl;

    cin>>first_number;

    cout<<"Enter the Second Number"<<endl;

    cin>>second_number;

    cout<<"Enter the Third Number"<<endl;

    cin>>third_number;

    if(first_number<=second_number && first_number<=third_number)
    {
        if(second_number<=third_number)
        {
            cout<<first_number<<", "<<third_number<<endl;
        }
        else
        {
            cout<<first_number<<", "<<second_number<<endl;
        }
    }
    else if(second_number<=first_number && second_number<=third_number)
    {
        if(first_number<=third_number)
        {
            cout<<second_number<<", "<<third_number<<endl;
        }
        else
        {
            cout<<second_number<<", "<<first_number<<endl;
        }
    }
    else
    {
        if(first_number<=second_number)
        {
            cout<<third_number<<", "<<second_number<<endl;
        }
        else
        {
            cout<<third_number<<", "<<first_number<<endl;
        }
    }

    return 0;
}
