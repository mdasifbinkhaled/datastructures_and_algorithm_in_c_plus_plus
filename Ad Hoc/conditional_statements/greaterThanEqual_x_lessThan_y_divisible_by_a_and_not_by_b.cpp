    /*
        write a program that checks if a number is within the range [x,y) and divisible
        by a but not by b

    */

    #include <iostream>

    using namespace std;

    int main(){

        int x,y,a,b, number;

        cout<<"Enter the Lower Bound"<<endl;
        cin>>x;

        cout<<"Enter the Upper Bound"<<endl;
        cin>>y;

        cout<<"Enter the First Divisor"<<endl;
        cin>>a;

        cout<<"Enter the Second Divisor"<<endl;
        cin>>b;

        cout<<"Enter a Number"<<endl;
        cin>>number;

        if( number>=x && number<y){

            cout<<number<<" is greater than or equal to "<<x<<" and less than "<<y;

            if(number%a==0 && number%b!=0){
                cout<<" and it is divisible "<<a<<" and not by "<<b<<endl;
            }else{
                cout<<" and it is not divisible "<<a<<" and not by "<<b<<endl;
            }
        }else{

            cout<<number<<" is not greater than or equal to "<<x<<" and less than "<<y<<endl;
        }

        return 0;
    }
