/*
    write a program that asks the user to enter the x and y coordinates of
    point in 2 dimensional space. Then find where the point is located.
    Possible locations are the four quadrants; the two axes and the origin.
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double x, y;

    cout<<"Enter the First Number"<<endl;
    cin>>x;

    cout<<"Enter the Second Number"<<endl;
    cin>>y;

    if(x>=0 && y>=0)
    {
        cout<<"("<<x<<", "<<y<<") is in the "<<"1st Quadrant"<<endl;
    }
    else if(x>=0 && y<0)
    {
        cout<<"("<<x<<", "<<y<<") is in the "<<"2nd Quadrant"<<endl;
    }
    else if(x<0 && y<0)
    {
        cout<<"("<<x<<", "<<y<<") is in the "<<"3rd Quadrant"<<endl;
    }
    else
    {
        cout<<"("<<x<<", "<<y<<") is in the "<<"4th Quadrant"<<endl;
    }



    return 0;
}
