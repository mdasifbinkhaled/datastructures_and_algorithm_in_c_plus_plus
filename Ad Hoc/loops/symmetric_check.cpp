/*
    write a program that takes a number from the user and checks whether
    it is symmetric or not. A number is symmetric if the reverse of that
    number is equal to the original.

*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int number,temporary, reversed=0;

    cout<<"Enter a Number"<<endl;

    cin>>number;

    temporary=number;

    while(temporary!=0)
    {
        reversed = reversed * 10 + temporary % 10;

        temporary=temporary/10;
    }

    if(reversed==number)
    {
        cout<<number<<" is a Symmetric Number"<<endl;
    }
    else
    {
        cout<<number<<" is not a Symmetric Number"<<endl;
    }

    return 0;
}

