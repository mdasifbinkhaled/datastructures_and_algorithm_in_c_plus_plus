/*
    write a program that takes two integers and print all
    the number between those two numbers.
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a, b;

    cout<<"Enter Starting Range"<<endl;

    cin>>a;

    cout<<"Enter Ending Range"<<endl;

    cin>>b;

    while(a<=b){

        cout<<a;

        if(a<b){
            cout<<", ";
        }

        a++;
    }

    return 0;
}



