/*
    write a program that displays the following character
    sequence. Make sure there is no extra comma
    a, b, c, ...... ,z
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{


    int number=97;

    while(number<=122){

        cout<<char(number);

        if(number<122){
            cout<<", ";
        }

        number++;
    }

    return 0;
}

