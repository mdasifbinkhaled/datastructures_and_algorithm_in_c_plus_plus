/*
    write a program that displays the following number
    sequence. Make sure there is no extra comma
    2, 2.25, 2.50, 2.75, ......, 4
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    double number=2;

    while(number<=4){

        cout<<number;

        if(number<4){
            cout<<", ";
        }

        number+=.25;
    }

    return 0;
}

