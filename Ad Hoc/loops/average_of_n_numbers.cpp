/*
    write a program that takes n number from the user and prints their average
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n,counter=0;

    double input, sum=0;

    cout<<"Enter Quantity"<<endl;
    cin>>n;

    while(counter<n)
    {
        cout<<"Enter Number"<<endl;
        cin>>input;

        sum = sum + input;

        counter++;
    }

    cout<<"Average of "<<n<<" numbers is "<<sum/n<<endl;

    return 0;
}
