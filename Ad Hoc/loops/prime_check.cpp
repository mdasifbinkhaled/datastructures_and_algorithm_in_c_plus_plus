/*
   write a program that take a number from the user and checks whether
   it is prime or not.
*/


#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    int number;

    cout<<"Enter a Number"<<endl;

    cin>>number;

    if(number<=1){
        cout<<"Invalid Input"<<endl;
        return 0;
    }

    bool prime_status=true;

    for(int i=2; i<=int(sqrt(number)); i++)
    {

        if(number%i==0)
        {
            prime_status=false;
            break;
        }
    }

    if(prime_status)
    {
        cout<<number<<" is a Prime Number"<<endl;
    }
    else
    {
        cout<<number<<" is not a Prime Number"<<endl;
    }

    return 0;
}
