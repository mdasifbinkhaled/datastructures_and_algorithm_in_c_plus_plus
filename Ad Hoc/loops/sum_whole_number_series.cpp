/*
    write a program that displays the following series and computes its sum.
    Display all the terms and the sum. Make sure there is no extra plus sign at the end.

    1 + 2 + 3 + ...... = ?
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;

    cout<<"Enter Number of Terms"<<endl;

    cin>>n;

    int number = 1, sum=0;

    while(number<=n)
    {
        cout<<number;

        sum = sum + number;

        if(number<n){
            cout<<" + ";
        }

        number++;
    }

    cout<<" = "<<sum;

    return 0;
}





