/*
    write a program that will reverse a given integer
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;

    cout<<"Enter Number"<<endl;

    cin>>n;

    int reversed=0, temporary =n;

    while(temporary!=0)
    {
        reversed = reversed * 10 + temporary % 10;

        temporary=temporary / 10;
    }

    cout<<n<<" reversed is "<<reversed<<endl;

    return 0;
}





