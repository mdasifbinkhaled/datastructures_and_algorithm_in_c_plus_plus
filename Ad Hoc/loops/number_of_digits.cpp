/*
    write a program that will count the number of digits in a given integer.
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;

    cout<<"Enter Number"<<endl;

    cin>>n;

    int counter=1, temporary =n;

    while(temporary>9)
    {
        temporary=temporary/10;

        counter++;
    }

    cout<<n<<" has "<<counter<<" digit(s)"<<endl;

    return 0;
}





