/*
    write a program that keeps taking input (integers)
    until the user enters -1
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int input;

    while(input!=-1){

        cout<<"Enter Number"<<endl;
        cin>>input;
    }

    return 0;
}

