/*
    write a program that takes a number from the user
    and prints that many asterisks
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;

    cout<<"Enter Quantity"<<endl;
    cin>>n;

    while(n--)
    {
        cout<<"*";
    }

    return 0;
}





