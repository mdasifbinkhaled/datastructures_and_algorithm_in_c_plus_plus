/*
    write a program that takes a number from the user and checks whether
    it is perfect or not. A number is considered perfect if the summation
    of all its positive divisor excluding that number itself is equal to that
    number

*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    int number, sum=0;

    cout<<"Enter a Number"<<endl;

    cin>>number;

    for(int i = 1;i<number;i++){
        if(number%i==0){
            sum=sum+i;
        }
    }

    if(sum==number)
    {
        cout<<number<<" is a Perfect Number"<<endl;
    }
    else
    {
        cout<<number<<" is not a Perfect Number"<<endl;
    }

    return 0;
}
