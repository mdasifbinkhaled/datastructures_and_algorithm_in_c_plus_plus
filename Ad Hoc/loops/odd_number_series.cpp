/*
    write a program that displays the following number
    sequence. Make sure there is no extra comma
    1, 2, 3, .......
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int input;

    int number=1;

    cout<<"Enter Range"<<endl;

    cin>>input;

    while(number<=input){

        cout<<number;

        if(number<input-1){
            cout<<", ";
        }

        number+=2;
    }

    return 0;
}


