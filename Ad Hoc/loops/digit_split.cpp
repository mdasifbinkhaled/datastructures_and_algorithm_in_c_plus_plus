/*
    write a program that takes a number from the user and prints each
    of the digits in a seperate line

*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int number, reversed=0;

    cout<<"Enter a Number"<<endl;

    cin>>number;

    while(number>9)
    {
        reversed = reversed * 10 + number % 10;

        number=number/10;
    }


    while(reversed>9)
    {
        cout<<reversed%10<<endl;

        reversed=reversed/10;
    }

    return 0;
}


