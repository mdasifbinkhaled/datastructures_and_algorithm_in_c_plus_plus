/*
    write a program that displays the following number
    sequence. Make sure there is no extra comma
    0, 1, 2, 3, ......
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int input;

    int number=0;

    cout<<"Enter Range"<<endl;

    cin>>input;

    while(number<=input){

        cout<<number;

        if(number<input){
            cout<<", ";
        }

        number++;
    }

    return 0;
}

