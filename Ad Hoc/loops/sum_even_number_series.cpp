/*
    write a program that displays the following series and computes its sum.
    Display all the terms and the sum. Make sure there is no extra plus sign at the end.

    2 + 4 + 6 + ...... = ?
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;

    cout<<"Enter Number of Terms"<<endl;

    cin>>n;

    int number = 2, sum=0;

    while(number<=n)
    {
        cout<<number;

        sum = sum + number;

        if(number<n-1){
            cout<<" + ";
        }

        number+=2;
    }

    cout<<" = "<<sum;

    return 0;
}





