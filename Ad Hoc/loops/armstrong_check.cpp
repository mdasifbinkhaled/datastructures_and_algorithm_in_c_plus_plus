/*
    write a program that takes a number from the user and checks whether
    it is an armstrong number or not. An armstrong number is an integer
    such that the sum of the cubes of its digits is equal to the number
    itself.

*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    int number, sum=0, temporary;

    cout<<"Enter a Number"<<endl;

    cin>>number;

    temporary=number;

    while(temporary!=0)
    {

        sum=sum+pow(temporary%10,3);

        temporary=temporary/10;
    }

    if(sum==number)
    {
        cout<<number<<" is an Armstrong Number"<<endl;
    }
    else
    {
        cout<<number<<" is not an Armstrong Number"<<endl;
    }

    return 0;
}
