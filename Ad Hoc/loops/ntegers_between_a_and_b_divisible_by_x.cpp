/*
    write a program that takes two integers and prints all the number
    that are divisible by x between them
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a, b, x;

    cout<<"Enter Starting Range"<<endl;

    cin>>a;

    cout<<"Enter Ending Range"<<endl;

    cin>>b;

    cout<<"Enter Divisor"<<endl;

    cin>>x;

    while(a<=b)
    {

        if(a%x==0)
        {

            cout<<a;

            if(a<b-1)
            {
                cout<<", ";
            }
        }


        a++;
    }

    return 0;
}




