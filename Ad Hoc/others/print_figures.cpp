/*

Print the following

(\__/)
 (='.'=)
 C(")_(")
 ,___,
 (6v6)
 (_^(_\
 " " \\
 "/\_/\
 >^,^<
 ""/ \
 '(___)_/
 @..@
 (----)
 ( >__< )
 ^^ ~~ ^^

*/


#include <iostream>

using namespace std;

int main(){

    cout<<"(\__/)"<<endl;
    cout<<"(='.'=)"<<endl;
    cout<<"C(\")_(\")"<<endl;
    cout<<",___,"<<endl;
    cout<<"(6v6)"<<endl;
    cout<<"(_^(_\\"<<endl;
    cout<<"\" \" \\\\"<<endl;
    cout<<"\"/\\_/\\"<<endl;
    cout<<" >^,^<"<<endl;
    cout<<"\"\"/ \\"<<endl;
    cout<<"'(___)_/"<<endl;
    cout<<"@..@"<<endl;
    cout<<"(----)"<<endl;
    cout<<"( >__< )"<<endl;
    cout<<"^^ ~~ ^^"<<endl;

    return 0;

}
