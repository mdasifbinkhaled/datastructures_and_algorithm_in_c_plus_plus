/*
Write a program that will take the length and width of a rectangle from the user and
prints the area and perimeter of the rectangle.
*/

#include <iostream>


using namespace std;


int main(){

    double height, base;

    cout<<"Enter the height of the rectangle"<<endl;

    cin>>height;


    cout<<"Enter the base of the rectangle"<<endl;

    cin>>base;


    double area = height*base;
    double perimeter = 2 * (height + base);

    cout<<"Area "<<area<<" Perimeter "<<perimeter<<endl;


    return 0;
}
