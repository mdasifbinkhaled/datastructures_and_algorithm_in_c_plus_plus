/*
    Write a program that takes a 3-digit positive integer from the user and then prints
    the reversed number. For example, if the user enters 289, the program prints 982.
*/

#include <iostream>

using namespace std;

int main(){

    int number, reversed, multiplyer=10;

    cout<<"Enter Number"<<endl;

    cin>>number;

    reversed = number % 10;
    number = number/10;

    while(number!=0){

        reversed = reversed * multiplyer + number % 10;

        number = number/10;

    }


    cout<< "Reversed "<<reversed<<endl;


    return 0;
}
