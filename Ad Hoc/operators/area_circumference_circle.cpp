/*
Write a program that takes the radius of a circle from the user and prints the area
and circumference of the circle.
*/

#include <iostream>
#include <cmath>


using namespace std;


int main(){

    double radius;

    cout<<"Enter the radius of the circle"<<endl;

    cin>>radius;

    double area = M_PI * radius * radius;
    double circumference = 2 * M_PI * radius;

    cout<<"Area "<<area<<" Circumference "<<circumference<<endl;


    return 0;
}
