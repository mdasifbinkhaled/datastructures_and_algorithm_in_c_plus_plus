/*
    Write a program that takes a decimal number from the user and then prints the
    integer part and the decimal part separately. For example, if the user enters 2.718,
    the program prints: Integer part = 2 and decimal part = .718.
*/

#include <iostream>

using namespace std;

int main(){

    int integer_portion;

    double number, decimal_portion;

    cout<<"Enter Number"<<endl;

    cin>>number;

    integer_portion = int(number);

    decimal_portion = number-integer_portion;

    cout<< "Integer Portion "<<integer_portion<<" Decimal Portion "<<decimal_portion<<endl;


    return 0;
}
