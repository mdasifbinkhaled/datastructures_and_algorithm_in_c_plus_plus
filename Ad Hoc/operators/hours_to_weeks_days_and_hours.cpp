/*
Write a program that takes the number of hours as input and displays the
equivalent number of weeks, days, and hours. For example, if the user inputs 4000
hours, the program displays 23 weeks, 5 days and 16 hours.
*/

#include <iostream>


using namespace std;


int main(){

    int hours;

    cout<<"Enter the number of Hours"<<endl;

    cin>>hours;

    double weeks = hours/(7*24);

    hours = hours%(7*24);

    double days = hours/24;

    hours = hours%30;

    cout<<weeks<<" Weeks "<<days<<" Days "<<hours<<" Hours"<<endl;


    return 0;
}
