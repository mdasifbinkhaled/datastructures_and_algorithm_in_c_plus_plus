/*
Write a program that takes a temperature input in Fahrenheit and displays the
temperature in Celsius and in Kelvin. Use the conversion formulae 5(F − 32) = 9C
and C = K − 273.15.
*/

#include <iostream>


using namespace std;


int main(){

    double fahrenheit;

    cout<<"Enter the temperature in Fahrenheit"<<endl;

    cin>>fahrenheit;

    double celsius = (5*(fahrenheit-32))/9;

    double kelvin = celsius + 273.15;

    cout<<"Celsius "<<celsius<<" Kelvin "<<kelvin<<endl;


    return 0;
}
