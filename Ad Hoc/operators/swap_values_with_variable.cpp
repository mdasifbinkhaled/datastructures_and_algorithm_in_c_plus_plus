/*
    Write a program that swaps (exchanges) the values of two variables.
*/

#include <iostream>

using namespace std;

int main(){

    int a,b,c;

    cout<<"Enter the First Number"<<endl;

    cin>>a;

    cout<<"Enter the Second Number"<<endl;

    cin>>b;

    cout<< "Before Swapping a "<<a<<" b "<<b<<endl;

    int temporary=a;

    a=b;

    b=temporary;

    cout<< "After Swapping a "<<a<<" b "<<b<<endl;


    return 0;
}
