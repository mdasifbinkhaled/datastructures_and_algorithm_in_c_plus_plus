/*
    Write a program that swaps the values of two variables without using a 3rd variable.
*/

#include <iostream>

using namespace std;

int main(){

    int a,b;

    cout<<"Enter the First Number"<<endl;

    cin>>a;

    cout<<"Enter the Second Number"<<endl;

    cin>>b;

    cout<< "Before Swapping a "<<a<<" b "<<b<<endl;

    a = a + b;

    b = a - b;

    a = a - b;

    cout<< "After Swapping a "<<a<<" b "<<b<<endl;


    return 0;
}
