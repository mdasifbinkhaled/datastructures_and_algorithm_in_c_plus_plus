/*
Write a program that converts the number of days into month and years. For
example, if the user inputs 813 days, the program prints: 2 years 2 months 23 days.
(don’t worry about leap year and you can calculate using 1 month = 30 days)
*/

#include <iostream>


using namespace std;


int main(){

    int days;

    cout<<"Enter the number of Days"<<endl;

    cin>>days;

    double years = days/365;

    days = days%365;

    double months = days/30;

    days = days%30;

    cout<<years<<" Years "<<months<<" Months "<<days<<" days"<<endl;


    return 0;
}
