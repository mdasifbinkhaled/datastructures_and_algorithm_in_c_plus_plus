/*
Write a program that takes the height of the user in meter and converts it to feet. (1
inch = 2.54 cm, 1 foot = 12 inch).
*/

#include <iostream>


using namespace std;


int main(){

    double height_in_meter;

    cout<<"Enter the height of the user in meter"<<endl;

    cin>>height_in_meter;

    double height_in_feet = height_in_meter * 3.28084;

    cout<<"Height in Feet "<<height_in_feet<<endl;


    return 0;
}
