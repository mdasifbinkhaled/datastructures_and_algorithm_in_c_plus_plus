
/*
    Take an integer from the user and print the last digit of that number. For example, if
    the user enters 10773, the program prints 3.
*/

#include <iostream>

using namespace std;

int main(){

    int number,last_digit;

    cout<<"Enter Number"<<endl;

    cin>>number;

    last_digit = number % 10;

    cout<< "Last Digit "<<last_digit<<endl;


    return 0;
}
