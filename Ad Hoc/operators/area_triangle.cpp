/*
Write a program that will take the height and base of triangle from the user and
prints the area of the triangle.
*/

#include <iostream>


using namespace std;


int main(){

    double height, base;

    cout<<"Enter the height of the triangle"<<endl;

    cin>>height;


    cout<<"Enter the base of the triangle"<<endl;

    cin>>base;


    double area = .5 * height * base;

    cout<<"Area "<<area<<endl;


    return 0;
}
