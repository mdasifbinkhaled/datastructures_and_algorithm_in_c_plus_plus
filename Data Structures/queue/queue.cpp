#include <iostream>
using namespace std;

struct node
{
    int value;
    node * next;
};

struct myqueue
{
    node * queue_front=NULL;
    node * queue_rear=NULL;
    int length=0;

    void enqueue(int value)
    {
        node * newNode=new node();
        newNode->value=value;
        if(queue_front==NULL)
        {
            queue_front=newNode;
            queue_rear=newNode;
        }
        else
        {
            queue_rear->next=newNode;
            queue_rear=newNode;
        }
        length++;
    }

    void dequeue()
    {
        if(queue_front!=NULL)
        {
            node * node=queue_front;
            queue_front=queue_front->next;

            node->next=NULL;
            delete node;

            if(queue_front==NULL)
            {
                queue_rear=NULL;
            }

            length--;
        }
    }

    bool isEmpty()
    {
        return length==0?true:false;
    }

    int getSize()
    {
        return length;
    }

    void display()
    {
        int i=0;
        for(node * current=queue_front; current!=NULL; current=current->next)
        {
            cout<<i<<" -> "<<current->value<<endl;
            i++;
        }
    }
};




int main()
{
    myqueue * q1=new myqueue();
    q1->display();
    cout<<"q1 isEmpty "<<q1->isEmpty()<<endl;
    q1->enqueue(45);
    q1->enqueue(412);
    q1->enqueue(15);
    q1->enqueue(35);
    q1->enqueue(25);
    cout<<"q1 size "<<q1->getSize()<<endl;
    cout<<"q1 isEmpty "<<q1->isEmpty()<<endl;
    q1->display();
    q1->dequeue();
    cout<<"q1 size "<<q1->getSize()<<endl;
    cout<<"q1 isEmpty "<<q1->isEmpty()<<endl;
    q1->display();
    q1->dequeue();
    cout<<"q1 size "<<q1->getSize()<<endl;
    cout<<"q1 isEmpty "<<q1->isEmpty()<<endl;
    q1->display();
    q1->dequeue();
    q1->dequeue();
    q1->dequeue();
    q1->dequeue();
    cout<<"q1 size "<<q1->getSize()<<endl;
    cout<<"q1 isEmpty "<<q1->isEmpty()<<endl;
    q1->display();
    myqueue * q2=new myqueue();
    q2->display();
    cout<<"q2 isEmpty "<<q2->isEmpty()<<endl;
    q2->enqueue(1);
    q2->enqueue(12);
    q2->enqueue(1);
    q2->enqueue(5);
    q2->enqueue(2);
    cout<<"q2 size "<<q2->getSize()<<endl;
    cout<<"q2 isEmpty "<<q2->isEmpty()<<endl;
    q2->display();

}
