#include <iostream>
#include <list>
#include <iterator>
using namespace std;

struct Data
{
    int value;
    int key;
    bool status;
};
struct HashTable
{
    int bucket;
    Data ** table;
};

int hashFunction(int bucketSize,int key,int probe)
{
    return (key+probe)%bucketSize;
}

void insertData(HashTable & hash_table,int key,int value)
{
    int probe=0;
    int index=hashFunction(hash_table.bucket,key,probe);

    while(hash_table.table[index]!=NULL && hash_table.table[index]->status!= 0 && hash_table.bucket!=(probe-1))
    {
        index=hashFunction(hash_table.bucket,key,++probe);
    }
    if(index <hash_table.bucket)
    {
        Data * data=new Data;
        data->key=key;
        data->value=value;
        data->status=1;
        if(hash_table.table[index]!=NULL)
        {
            delete hash_table.table[index];
        }
        hash_table.table[index]=data;
    }
}
void deleteData(HashTable & hash_table,int key)
{
    int probe=0;
    int index=hashFunction(hash_table.bucket,key,probe);
    while(hash_table.table[index]!=NULL && hash_table.table[index]->key!= key && hash_table.bucket!=(probe-1))
    {
        index=hashFunction(hash_table.bucket,key,++probe);
    }
    if(hash_table.table[index]!=NULL)
    {
        hash_table.table[index]->status=0;
    }
}

Data * searchData(HashTable hash_table,int key)
{
    int probe=0;
    int index=hashFunction(hash_table.bucket,key,probe);
    while(hash_table.table[index]!=NULL && hash_table.table[index]->key!= key && hash_table.bucket!=(probe-1))
    {
        index=hashFunction(hash_table.bucket,key,++probe);
    }
    if(hash_table.table[index]!=NULL && hash_table.table[index]->status==1)
    {
        return hash_table.table[index];
    }
    return NULL;
}

void display(HashTable hash_table)
{
    cout<<"index -> (key, value)"<<endl;
    for(int i=0; i<hash_table.bucket; i++)
    {
        cout<<i;
        if(hash_table.table[i]!=NULL && hash_table.table[i]->status==1)
        {
            cout<<" -> ("<<hash_table.table[i]->key<<", "<<hash_table.table[i]->value<<") "<<endl;
        }
        else
        {
            cout<<" -> NULL"<<endl;
        }

    }
}
int main()
{
    int value[]= {234,234,234,34,425,234};
    int key[]= {46,12,882,132,79,6};
    int n=sizeof(value)/sizeof(value[0]);
    HashTable hash_table;
    hash_table.bucket=7;
    hash_table.table=new Data *[hash_table.bucket];
    for(int i=0; i<n; i++)
    {
        insertData(hash_table,key[i],value[i]);
    }
    display(hash_table);
    deleteData(hash_table,882);
    display(hash_table);
    insertData(hash_table,20,12);
    display(hash_table);
    deleteData(hash_table,132);
    deleteData(hash_table,46);
    deleteData(hash_table,882);
    deleteData(hash_table,12);
    deleteData(hash_table,132);
    deleteData(hash_table,79);
    deleteData(hash_table,6);
    deleteData(hash_table,6);
    insertData(hash_table,53,10);
    display(hash_table);
    cout<<searchData(hash_table,20)->value<<endl;

    return 0;
}
