#include <iostream>
#include <list>
#include <iterator>
using namespace std;

struct Data
{
    int value;
    int key;
};
struct HashTable
{
    int bucket;
    list <Data*> * table;
};

int hashFunction(int bucketSize,int key)
{
    return key%bucketSize;
}

void insertData(HashTable & hash_table,int key,int value)
{
    Data * data=new Data;
    data->key=key;
    data->value=value;
    int index=hashFunction(hash_table.bucket,key);
    hash_table.table[index].push_front(data);
}
void deleteData(HashTable & hash_table,int key)
{
    int index=hashFunction(hash_table.bucket,key);
    list<Data *> :: iterator list_iterator;
    for(list_iterator=hash_table.table[index].begin(); list_iterator!=hash_table.table[index].end(); list_iterator++)
    {
        if((*list_iterator)->key==key)
        {
            hash_table.table[index].erase(list_iterator);
            break;
        }
    }
}

Data * searchData(HashTable hash_table,int key)
{
    int index=hashFunction(hash_table.bucket,key);
    list<Data *> :: iterator list_iterator;
    for(list_iterator=hash_table.table[index].begin(); list_iterator!=hash_table.table[index].end(); list_iterator++)
    {
        if((*list_iterator)->key==key)
        {
            return *list_iterator;

        }
    }
    return NULL;
}

void display(HashTable hash_table)
{
    cout<<"index -> (key, value)"<<endl;
    for(int i=0; i<hash_table.bucket; i++)
    {
        cout<<i<<" ";
        for (auto data:hash_table.table[i])
        {
            cout<<"-> ("<<data->key<<", "<<data->value<<") ";
        }
        cout<<endl;
    }
}
int main()
{
    int value[]= {234,234,234,34,425,234};
    int key[]= {46,12,882,132,79,6};
    int n=sizeof(value)/sizeof(value[0]);
    HashTable hash_table;
    hash_table.bucket=10;
    hash_table.table=new list<Data*>[hash_table.bucket];
    for(int i=0; i<n; i++)
    {
        insertData(hash_table,key[i],value[i]);
    }
    display(hash_table);
    deleteData(hash_table,882);
    display(hash_table);
    cout<<searchData(hash_table,12)->value<<endl;

    return 0;
}
