#include <iostream>
#include <list>
#include <iterator>
using namespace std;

struct Data
{
    int value;
    int key;
    Data * next;
};
struct HashTable
{
    int bucket;
    Data ** table;
};

int hashFunction(int bucketSize,int key)
{
    return key%bucketSize;
}

void insertData(HashTable & hash_table,int key,int value)
{
    Data * data=new Data;
    data->key=key;
    data->value=value;
    data->next=NULL;
    int index=hashFunction(hash_table.bucket,key);
    if(hash_table.table[index]==NULL)
    {
        hash_table.table[index]=data;
    }
    else
    {
        data->next=hash_table.table[index];
        hash_table.table[index]=data;
    }
}
void deleteData(HashTable & hash_table,int key)
{
    int index=hashFunction(hash_table.bucket,key);
    if(hash_table.table[index]!=NULL)
    {

        if(hash_table.table[index]->key==key)
        {
            Data * node=hash_table.table[index];
            hash_table.table[index]=hash_table.table[index]->next;
            delete node;
        }
        else
        {
            for(Data * predecessor=hash_table.table[index]; predecessor->next!=NULL; predecessor=predecessor->next)
            {
                if(predecessor->next->key==key)
                {
                    Data * node=predecessor->next;
                    predecessor->next=predecessor->next->next;
                    delete node;
                    break;
                }
            }
        }
    }
}

Data * searchData(HashTable hash_table,int key)
{
    int index=hashFunction(hash_table.bucket,key);

    if(hash_table.table[index]!=NULL)
    {

        if(hash_table.table[index]->key==key)
        {
            return hash_table.table[index];
        }
        else
        {
            for(Data * current=hash_table.table[index]; current!=NULL; current=current->next)
            {
                if(current->key==key)
                {
                    return current;
                }
            }
        }
    }
    return NULL;
}

void display(HashTable hash_table)
{
    cout<<"index -> (key, value)"<<endl;
    for(int i=0; i<hash_table.bucket; i++)
    {
        cout<<i<<" ";
        for (Data * current=hash_table.table[i]; current!=NULL; current=current->next)
        {
            cout<<"-> ("<<current->key<<", "<<current->value<<")";
        }
        cout<<"-> NULL";
        cout<<endl;
    }
}
int main()
{
    int value[]= {234,234,234,34,425,234};
    int key[]= {46,12,882,132,79,6};
    int n=sizeof(value)/sizeof(value[0]);
    HashTable hash_table;
    hash_table.bucket=10;
    hash_table.table=new Data *[hash_table.bucket];
    for(int i=0; i<n; i++)
    {
        insertData(hash_table,key[i],value[i]);
    }
    display(hash_table);
    deleteData(hash_table,46);
    deleteData(hash_table,882);
    deleteData(hash_table,12);
    deleteData(hash_table,132);
    deleteData(hash_table,79);
    deleteData(hash_table,6);
    deleteData(hash_table,6);
    insertData(hash_table,53,10);
    display(hash_table);
    cout<<searchData(hash_table,53)->value<<endl;

    return 0;
}
