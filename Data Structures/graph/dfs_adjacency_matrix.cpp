#include <iostream>
#include <stack>
#include <climits>
#include <cfloat>
using namespace std;

void addEdge(double **adjacencyMatrix, int source, int destination, double weight)
{
    adjacencyMatrix[source][destination] = weight;
}
void displayMatrix(double **adjacencyMatrix, int V)
{
    for (int i = 0; i < V; i++)
    {
        for (int j = 0; j < V; j++)
        {
            cout << adjacencyMatrix[i][j] << " ";
        }
        cout << endl;
    }
}
void displayParentAndDistance(int *parent, double *distance, int n)
{
    for (int i = 0; i < n; i++)
    {
        cout << "Vertex " << i << " Parent " << parent[i] << " Distance " << distance[i] << endl;
    }
}

void dfsVisit(double **adjacencyMatrix, int V, int source, int destination, int *parent, double *distance, bool *visited)
{
    visited[source] = true;

    for (int i = 0; i < V; i++)
    {
        if (adjacencyMatrix[source][i] != 0 && visited[i] == false)
        {

            distance[i] = distance[source] + adjacencyMatrix[source][i];
            parent[i] = source;
            dfsVisit(adjacencyMatrix, V, i, destination, parent, distance, visited);
        }
    }
}

void dfs(double **adjacencyMatrix, int V, int source, int destination)
{
    int *parent = new int[V];
    double *distance = new double[V];
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
    {
        parent[i] = -1;
        distance[i] = DBL_MAX;
        visited[i] = false;
    }

    int vertex_order = source;

    for (int i = 0; i < V; i++)
    {
        if (visited[vertex_order] == false)
        {
            distance[vertex_order] = 0;
            dfsVisit(adjacencyMatrix, V, vertex_order, destination, parent, distance, visited);
        }
        vertex_order = (vertex_order + 1) % V;
    }

    displayParentAndDistance(parent, distance, V);
}

int main()
{

    int V = 9;
    double **adjacencyMatrix = new double *[V];
    for (int i = 0; i < V; i++)
    {
        adjacencyMatrix[i] = new double[V]{0};
    }
    cout << "Empty Matrix" << endl;
    displayMatrix(adjacencyMatrix, V);
    addEdge(adjacencyMatrix, 0, 1, 1);
    addEdge(adjacencyMatrix, 0, 8, 1);
    addEdge(adjacencyMatrix, 0, 7, 1);
    addEdge(adjacencyMatrix, 1, 8, 1);
    addEdge(adjacencyMatrix, 2, 4, 1);
    addEdge(adjacencyMatrix, 2, 8, 1);
    addEdge(adjacencyMatrix, 3, 2, 1);
    addEdge(adjacencyMatrix, 5, 3, 1);
    addEdge(adjacencyMatrix, 5, 4, 1);
    addEdge(adjacencyMatrix, 5, 8, 1);
    addEdge(adjacencyMatrix, 6, 5, 1);
    addEdge(adjacencyMatrix, 7, 5, 1);
    addEdge(adjacencyMatrix, 7, 6, 1);
    addEdge(adjacencyMatrix, 8, 3, 1);
    cout << "All Edges Updated" << endl;
    displayMatrix(adjacencyMatrix, V);
    dfs(adjacencyMatrix, V, 0, 4);
}
