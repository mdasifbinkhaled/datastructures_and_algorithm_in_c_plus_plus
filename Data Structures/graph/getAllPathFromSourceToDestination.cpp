#include <iostream>
#include <queue>
#include <cstring>
using namespace std;

void addEdge(int ** adjacencyMatrix,int source,int destination,int weight)
{
    adjacencyMatrix[source][destination]=weight;
}
void displayMatrix(int ** adjacencyMatrix,int V)
{
    for(int i=0; i<V; i++)
    {
        for(int j=0; j<V; j++)
        {
            cout<<adjacencyMatrix[i][j]<<" ";
        }
        cout<<endl;
    }

}
void displayParentAndDistance(int * parent,int * distance, int n)
{
    for (int i = 0; i < n; i++)
    {
        cout<<"Vertex "<<i<<" Parent "<< parent[i]<<" Distance "<<distance[i]<<endl;
    }

}

void getAllPathFromSourceToDestination(int ** adjacencyMatrix,int V,int source, int destination,bool visited [],string path)
{
    if(visited==NULL)
    {
        visited =new bool [V] {false};
    }
    else
    {
        visited[source]=true;
    }

    if(source==destination)
    {
        cout<<path<<endl;
    }
    else
    {
        for(int i=0; i<V; i++)
        {
            string recurse_path;
            if(adjacencyMatrix[source][i]!=0 && visited[i]==false)
            {
                recurse_path=path;
                recurse_path.append(" ");
                recurse_path.append(to_string(i));
                getAllPathFromSourceToDestination(adjacencyMatrix,V,i,destination,visited,recurse_path);
            }
        }
    }
    visited[source]=false;
}


int main()
{

    int V=9;
    int ** adjacencyMatrix=new int *[V];
    for(int i=0; i<V; i++)
    {
        adjacencyMatrix[i]=new int [V] {0};
    }
    cout<<"Empty Matrix"<<endl;
    displayMatrix(adjacencyMatrix,V);
    addEdge(adjacencyMatrix,0,1,1);
    addEdge(adjacencyMatrix,0,8,1);
    addEdge(adjacencyMatrix,0,7,1);
    addEdge(adjacencyMatrix,1,8,1);
    addEdge(adjacencyMatrix,2,4,1);
    addEdge(adjacencyMatrix,2,8,1);
    addEdge(adjacencyMatrix,3,2,1);
    addEdge(adjacencyMatrix,5,3,1);
    addEdge(adjacencyMatrix,5,4,1);
    addEdge(adjacencyMatrix,5,8,1);
    addEdge(adjacencyMatrix,6,5,1);
    addEdge(adjacencyMatrix,7,5,1);
    addEdge(adjacencyMatrix,7,6,1);
    addEdge(adjacencyMatrix,8,3,1);
    cout<<"All Edges Updated"<<endl;
    displayMatrix(adjacencyMatrix,V);
    getAllPathFromSourceToDestination(adjacencyMatrix,V,0,8,NULL,to_string(0));
}

