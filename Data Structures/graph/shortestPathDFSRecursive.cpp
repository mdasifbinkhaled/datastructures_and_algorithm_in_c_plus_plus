#include <iostream>
#include <stack>
#include <climits>
using namespace std;

void addEdge(int ** adjacencyMatrix,int source,int destination,int weight)
{
    adjacencyMatrix[source][destination]=weight;
}
void displayMatrix(int ** adjacencyMatrix,int V)
{
    for(int i=0; i<V; i++)
    {
        for(int j=0; j<V; j++)
        {
            cout<<adjacencyMatrix[i][j]<<" ";
        }
        cout<<endl;
    }

}
void displayParentAndDistance(int * parent,int * distance, int n)
{
    for (int i = 0; i < n; i++)
    {
        cout<<"Vertex "<<i<<" Parent "<< parent[i]<<" Distance "<<distance[i]<<endl;
    }

}

void shortestPathDFSRecursiveVisit(int ** adjacencyMatrix,int V,int source, int destination,int * parent, int * distance, bool * visited)
{
    visited[source]=true;

    for(int i=0; i<V; i++)
    {

        if(adjacencyMatrix[source][i]!=0 && visited[i]==false)
        {
            distance[i]=distance[source]+1;
            parent[i]=source;
            shortestPathDFSRecursiveVisit(adjacencyMatrix,V,i,destination,parent,distance,visited);
        }else if(adjacencyMatrix[source][i]!=0 && (distance[source]+1)<distance[i]){
            distance[i]=distance[source]+1;
            parent[i]=source;
        }
    }

}

void shortestPathDFSRecursive(int ** adjacencyMatrix,int V,int source, int destination)
{
    int * parent=new int [V] {-1};
    int * distance=new int [V] {INT_MAX};
    bool * visited =new bool [V] {false};

    distance[source]=0;

    for(int i=0; i<V; i++)
    {
        if(visited[i]==false)
        {
            shortestPathDFSRecursiveVisit(adjacencyMatrix,V,i,destination,parent,distance,visited);
        }
    }


    displayParentAndDistance(parent,distance,V);

}

int main()
{

    int V=9;
    int ** adjacencyMatrix=new int *[V];
    for(int i=0; i<V; i++)
    {
        adjacencyMatrix[i]=new int [V] {0};
    }
    cout<<"Empty Matrix"<<endl;
    displayMatrix(adjacencyMatrix,V);
    addEdge(adjacencyMatrix,0,1,1);
    addEdge(adjacencyMatrix,0,8,1);
    addEdge(adjacencyMatrix,0,7,1);
    addEdge(adjacencyMatrix,1,8,1);
    addEdge(adjacencyMatrix,2,4,1);
    addEdge(adjacencyMatrix,2,8,1);
    addEdge(adjacencyMatrix,3,2,1);
    addEdge(adjacencyMatrix,5,3,1);
    addEdge(adjacencyMatrix,5,4,1);
    addEdge(adjacencyMatrix,5,8,1);
    addEdge(adjacencyMatrix,6,5,1);
    addEdge(adjacencyMatrix,7,5,1);
    addEdge(adjacencyMatrix,7,6,1);
    addEdge(adjacencyMatrix,8,3,1);
    cout<<"All Edges Updated"<<endl;
    displayMatrix(adjacencyMatrix,V);
    shortestPathDFSRecursive(adjacencyMatrix,V,0,4);
}

