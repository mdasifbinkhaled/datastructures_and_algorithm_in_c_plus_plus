#include <iostream>
#include <queue>
using namespace std;

void addEdge(int ** adjacencyMatrix,int source,int destination,int weight)
{
    adjacencyMatrix[source][destination]=weight;
}
void displayMatrix(int ** adjacencyMatrix,int V)
{
    for(int i=0; i<V; i++)
    {
        for(int j=0; j<V; j++)
        {
            cout<<adjacencyMatrix[i][j]<<" ";
        }
        cout<<endl;
    }

}

bool isReachable(int ** adjacencyMatrix,int V,int source, int destination)
{
    int * parent=new int [V] {0};
    bool * visited =new bool [V] {false};

    queue <int> q;

    q.push(source);

    parent[source]=-1;
    visited [source]=true;

    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0; i<V; i++)
        {
            if(adjacencyMatrix[u][i]!=0 && visited[i]==false)
            {
                visited[i]=true;
                parent[i]=u;
                q.push(i);
            }
        }
    }

    int current=destination;
    bool reachable=false;
    int c=0;
    while(current!=source)
    {
        current=parent[current];
        c++;
        if(current==source){
            reachable=true;
            break;
        }else if(c==V-1){
            break;
        }
    }
    return reachable;
}


int main()
{

    int V=9;
    int ** adjacencyMatrix=new int *[V];
    for(int i=0; i<V; i++)
    {
        adjacencyMatrix[i]=new int [V] {0};
    }
    cout<<"Empty Matrix"<<endl;
    displayMatrix(adjacencyMatrix,V);
    addEdge(adjacencyMatrix,0,1,1);
    addEdge(adjacencyMatrix,0,8,1);
    addEdge(adjacencyMatrix,0,7,1);
    addEdge(adjacencyMatrix,1,8,1);
    addEdge(adjacencyMatrix,2,4,1);
    addEdge(adjacencyMatrix,2,8,1);
    addEdge(adjacencyMatrix,3,2,1);
    addEdge(adjacencyMatrix,5,3,1);
    addEdge(adjacencyMatrix,5,4,1);
    addEdge(adjacencyMatrix,5,8,1);
    addEdge(adjacencyMatrix,6,5,1);
    addEdge(adjacencyMatrix,7,5,1);
    addEdge(adjacencyMatrix,7,6,1);
    addEdge(adjacencyMatrix,8,3,1);
    cout<<"All Edges Updated"<<endl;
    displayMatrix(adjacencyMatrix,V);
    cout<<isReachable(adjacencyMatrix,V,2,4)<<endl;
}
