#include <iostream>
#include <queue>
using namespace std;

void addEdge(int ** adjacencyMatrix,int source,int destination,int weight)
{
    adjacencyMatrix[source][destination]=weight;
}
void displayMatrix(int ** adjacencyMatrix,int V)
{
    for(int i=0; i<V; i++)
    {
        for(int j=0; j<V; j++)
        {
            cout<<adjacencyMatrix[i][j]<<" ";
        }
        cout<<endl;
    }

}

int main()
{

    int V=9;
    int ** adjacencyMatrix=new int *[V];
    for(int i=0; i<V; i++)
    {
        adjacencyMatrix[i]=new int [V] {0};
    }
    cout<<"Empty Matrix"<<endl;
    displayMatrix(adjacencyMatrix,V);
    addEdge(adjacencyMatrix,0,1,1);
    addEdge(adjacencyMatrix,0,8,1);
    addEdge(adjacencyMatrix,0,7,1);
    addEdge(adjacencyMatrix,1,8,1);
    addEdge(adjacencyMatrix,2,4,1);
    addEdge(adjacencyMatrix,2,8,1);
    addEdge(adjacencyMatrix,3,2,1);
    addEdge(adjacencyMatrix,5,3,1);
    addEdge(adjacencyMatrix,5,4,1);
    addEdge(adjacencyMatrix,5,8,1);
    addEdge(adjacencyMatrix,6,5,1);
    addEdge(adjacencyMatrix,7,5,1);
    addEdge(adjacencyMatrix,7,6,1);
    addEdge(adjacencyMatrix,8,3,1);
    cout<<"All Edges Updated"<<endl;
    displayMatrix(adjacencyMatrix,V);
}
