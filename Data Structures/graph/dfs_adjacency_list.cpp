#include <iostream>
#include <stack>
#include <climits>
#include <vector>
#include <cfloat>

using namespace std;

struct node
{
    node *parent = NULL;
    char color = 'W';
    int id;
    bool visited = false;
    double distance = DBL_MAX;
    int starting_time;
    int finishing_time;
    vector<pair<node *, double>> list;
};

void addEdge(vector<node *> adjacencyList, int source, int destination, double weight)
{
    adjacencyList[source]->list.push_back(make_pair(adjacencyList[destination], weight));
}
void display(vector<node *> adjacencyList)
{
    cout << "Source Vertex->(Destination Vertex, Vertex Cost)" << endl;
    for (int i = 0; i < adjacencyList.size(); i++)
    {
        cout << i;
        for (int j = 0; j < adjacencyList[i]->list.size(); j++)
        {
            cout << " -> (" << adjacencyList[i]->list[j].first->id << ", " << adjacencyList[i]->list[j].second << ")";
        }
        if (adjacencyList[i]->list.size() == 0)
        {
            cout << " -> NULL";
        }
        cout << endl;
    }
}
void displayParentAndDistance(vector<node *> adjacencyList)
{

    for (int i = 0; i < adjacencyList.size(); i++)
    {
        if (adjacencyList[i]->parent != NULL)
        {
            cout << "Vertex " << adjacencyList[i]->id << " Parent " << adjacencyList[i]->parent->id << " Distance " << adjacencyList[i]->distance << endl;
        }
        else
        {
            cout << "Vertex " << adjacencyList[i]->id << " Parent " << "NULL" << " Distance " << adjacencyList[i]->distance << endl;
        }
    }
}

void dfsVisit(vector<node *> adjacencyList, int source, int destination, int *time)
{

    *time++;
    adjacencyList[source]->starting_time = *time;
    adjacencyList[source]->visited = true;
    adjacencyList[source]->color = 'G';
    for (int i = 0; i < adjacencyList[source]->list.size(); i++)
    {
        if (adjacencyList[source]->list[i].first->visited == false)
        {
            adjacencyList[source]->list[i].first->distance = adjacencyList[source]->distance + adjacencyList[source]->list[i].second;
            adjacencyList[source]->list[i].first->parent = adjacencyList[source];
            dfsVisit(adjacencyList, adjacencyList[source]->list[i].first->id, destination, time);
        }
    }
    adjacencyList[source]->color = 'B';
    *time++;
    adjacencyList[source]->finishing_time = *time;
}

void dfs(vector<node *> adjacencyList, int source, int destination)
{

    
    int *time = new int(0);
     int vertex_order = source;
    for (int i = 0; i < adjacencyList.size(); i++)
    {
        
        if (adjacencyList[vertex_order]->visited == false)
        {
            adjacencyList[vertex_order]->distance = 0;
            dfsVisit(adjacencyList, vertex_order, destination, time);
        }
         vertex_order = (vertex_order + 1) % adjacencyList.size();
    }

    displayParentAndDistance(adjacencyList);
}

int main()
{

    int V = 9;
    vector<node *> adjacencyList;
    for (int i = 0; i < V; i++)
    {
        adjacencyList.push_back(new node);
        adjacencyList[i]->id = i;
    }
    cout << "Empty Matrix" << endl;
    display(adjacencyList);
    addEdge(adjacencyList, 0, 1, 1);
    addEdge(adjacencyList, 0, 8, 1);
    addEdge(adjacencyList, 0, 7, 1);
    addEdge(adjacencyList, 1, 8, 1);
    addEdge(adjacencyList, 2, 4, 1);
    addEdge(adjacencyList, 2, 8, 1);
    addEdge(adjacencyList, 3, 2, 1);
    addEdge(adjacencyList, 5, 3, 1);
    addEdge(adjacencyList, 5, 4, 1);
    addEdge(adjacencyList, 5, 8, 1);
    addEdge(adjacencyList, 6, 5, 1);
    addEdge(adjacencyList, 7, 5, 1);
    addEdge(adjacencyList, 7, 6, 1);
    addEdge(adjacencyList, 8, 3, 1);
    cout << "All Edges Updated" << endl;
    display(adjacencyList);
    dfs(adjacencyList, 0, 4);
}
