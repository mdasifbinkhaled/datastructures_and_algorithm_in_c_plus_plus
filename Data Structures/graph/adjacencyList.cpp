#include <iostream>
#include <vector>

using namespace std;

void addEdge(vector<pair<int,int>> * adjacencyList,int source,int destination,int weight)
{
    adjacencyList[source].push_back(make_pair(destination,weight));
}
void displayMatrix(vector<pair<int,int>>* adjacencyList,int V)
{
    for(int i=0; i<V; i++)
    {
        cout<<i<<" ";
        for(int j=0; j<adjacencyList[i].size(); j++)
        {
            cout<<"-> ("<<adjacencyList[i][j].first<<", "<<adjacencyList[i][j].second<<") ";
        }
        if(adjacencyList[i].size()==0)
        {
            cout<<"-> NULL";
        }
        cout<<endl;
    }

}

int main()
{

    int V=9;
    vector<pair<int,int>> * adjacencyList=new vector<pair<int,int>> [V];

    cout<<"Empty Matrix"<<endl;
    displayMatrix(adjacencyList,V);
    addEdge(adjacencyList,0,1,1);
    addEdge(adjacencyList,0,8,1);
    addEdge(adjacencyList,0,7,1);
    addEdge(adjacencyList,1,8,1);
    addEdge(adjacencyList,2,4,1);
    addEdge(adjacencyList,2,8,1);
    addEdge(adjacencyList,3,2,1);
    addEdge(adjacencyList,5,3,1);
    addEdge(adjacencyList,5,4,1);
    addEdge(adjacencyList,5,8,1);
    addEdge(adjacencyList,6,5,1);
    addEdge(adjacencyList,7,5,1);
    addEdge(adjacencyList,7,6,1);
    addEdge(adjacencyList,8,3,1);
    cout<<"All Edges Updated"<<endl;
    displayMatrix(adjacencyList,V);
}

