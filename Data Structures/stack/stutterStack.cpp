/*
Write a recursive function named stutterStack that accepts a
Stack of integers as a parameter and replaces every value in the
stack with two occurrences of that value. For example, suppose a
stack named s stores these values, from bottom =&gt; top:
{13, 27, 1, -4, 0, 9}
Then the call of stutterstack(s); should change the stack to
store the following values:
{13, 13, 27, 27, 1, 1, -4, -4, 0, 0, 9, 9}
Notice that you must preserve the original order. In the original
stack the 9 was at the top and would have been popped first. In
the new stack the two 9s would be the first values popped from
the stack. If the original stack is empty, the result should be
empty as well.
*/
#include <iostream>
#include <stack>

using namespace std;

void stutterStack(stack <int> & s){
    if(s.empty()){
        return;
    }
    int top=s.top();
    s.pop();
    stutterStack(s);
    s.push(top);
    s.push(top);
}

void display(stack<int> s)
{
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;
}

int main()
{
    stack<int> s;
    s.push(1);
    s.push(2);
    s.push(5);
    s.push(3);
    display(s);
    stutterStack(s);
    display(s);
}