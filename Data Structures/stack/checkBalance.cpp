/*
Write a function named checkBalance that accepts a string of
source code and uses a Stack to check whether the
braces/parentheses are balanced. Every ( or { must be closed by a
} or ) in the opposite order. Return the index at which an
imbalance occurs, or -1 if the string is balanced. If any ( or {
are never closed, return the string&#39;s length.
Here are some example calls:
// index 0123456789012345678901234567890

Constraints: Use a single stack as auxiliary storage.
*/
#include <iostream>
#include <stack>

using namespace std;

int checkBalance(string str)
{
    stack<char> s;
    for (int i = 0; str[i]; i++)
    {
        if (str[i] == '(' || str[i] == '{' || str[i] == '[')
        {
            s.push(str[i]);
        }
        else if (str[i] == ')' || str[i] == '}' || str[i] == ']')
        {
            char top = s.top();
            s.pop();
            if (str[i] == ')' && top != '(')
            {
                return i;
            }
            else if (str[i] == '}' && top != '{')
            {
                return i;
            }
            else if (str[i] == ']' && top != '[')
            {
                return i;
            }
        }
    }
    if (s.empty())
    {
        return -1;
    }
    return str.size();
}

int main()
{
    cout<<checkBalance("3+4654+549+(2+6)+{546+1}")<<endl;
    cout<<checkBalance("3+4654+549+(2+{6)+{546+1}")<<endl;
}