#include <iostream>
#include <stack>

using namespace std;

bool is01n10n(string str)
{

    stack<char> s;

    for (int i = 0; str[i]; i+=2)
    {
        if (str[i] == '0' && str[i + 1] == '1')
        {
            s.push(str[i]);
            s.push(str[i + 1]);
        }
        else if (str[i] == '1' && str[i + 1] == '0')
        {
            if (s.empty())
            {
                return false;
            }
            char first_top = s.top();
            s.pop();
            char second_top = s.top();
            s.pop();

            if (str[i] != first_top || str[i + 1] != second_top)
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    return s.empty() ? true : false;
}

int main()
{
    cout << is01n10n("alpha") << endl;
    cout << is01n10n("0110") << endl;
    cout << is01n10n("010101101010") << endl;
    cout << is01n10n("0101101010") << endl;
    cout << is01n10n("0101011010") << endl;
    cout << is01n10n("101010010101") << endl;
    cout << is01n10n("") << endl;
    return 0;
}
