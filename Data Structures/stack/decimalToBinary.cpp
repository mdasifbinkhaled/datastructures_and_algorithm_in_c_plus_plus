/*
Given a number n, write a function that generates and prints all
binary numbers with decimal values from 1 to n. You need to use a
queue for that .
Input:
5
Output:
0
01
10
11
100
101
Input:
3
Output:
0
01
10
11

Input:
1
Output:
0
01
*/
#include <iostream>
#include <stack>

using namespace std;

void decimalToBinary(int n){
    if(n==0){
        return;
    }

}

void display(stack<int> s)
{
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;
}

int main()
{
    stack<int> s;
    s.push(1);
    s.push(2);
    s.push(5);
    s.push(3);
    display(s);
    stutterStack(s);
    display(s);
}