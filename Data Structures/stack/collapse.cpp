/*
Write a function named collapse that takes a reference to a stack
of integers as a parameter and that collapses it by replacing
each successive pair of integers with the sum of the pair. For
example, suppose a stack stores these values (shown from bottom →
top):
{7, 2, 8, 9, 4, 13, 7, 1, 9, 10}
The first pair should be collapsed into 9 (7 + 2), the second
pair should be collapsed into 17 (8 + 9), the third pair should
be collapsed into 17 (4 + 13) and so on to yield:
{9, 17, 17, 8, 19}
If the stack stores an odd number of elements, the final element
is not collapsed. For example, the stack:
{1, 2, 3, 4, 5}
would collapse into:
{3, 7, 5}
With the 5 at the top of the stack unchanged. You may use one
stack or queue as auxiliary storage.
*/
#include <iostream>
#include <stack>

using namespace std;

void collapse(stack<int> &s)
{
    if (s.size() < 2)
    {
        return;
    }
    //update this implementation
    if (s.size() % 2 != 0)
    {
        s.push(0);
    }
    int first_top = s.top();
    s.pop();
    int second_top = s.top();
    s.pop();
    collapse(s);
    s.push(first_top + second_top);
}

void display(stack<int> s)
{
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;
}

int main()
{
    stack<int> s1;

    s1.push(7);
    s1.push(2);
    s1.push(8);
    s1.push(9);
    s1.push(4);
    s1.push(13);
    s1.push(7);
    s1.push(1);
    s1.push(9);
    s1.push(10);
    display(s1);
    collapse(s1);
    display(s1);
    stack<int> s2;
    s2.push(1);
    s2.push(2);
    s2.push(3);
    s2.push(4);
    s2.push(5);
    display(s2);
    collapse(s2);
    display(s2);
}
