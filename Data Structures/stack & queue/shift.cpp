/*
Write a function named shift that takes a reference to a stack of
integers and an integer n as parameters and that shifts n values
from the bottom of the stack to the top of the stack. For
example, if a variable called s stores the following sequence of
values:
{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

If we make the call shift(s, 6); the function shifts the six
values at the bottom of the stack to the top of the stack and
leaves the other values in the same order producing:
{7, 8, 9, 10, 6, 5, 4, 3, 2, 1}
Notice that the value that was at the bottom of the stack is now
at the top, the value that was second from the bottom is now
second from the top, the value that was third from the bottom is
now third from the top, and so on, and that the four values not
involved in the shift are now at the bottom of the stack in their
original order. If s had stored these values instead:
{7, 23, -7, 0, 22, -8, 4, 5}
If we make the following call: shift(s, 3); then s should store
these values after the call:
{0, 22, -8, 4, 5, -7, 23, 7}
You are to use one queue as auxiliary storage to solve this
problem. You may assume that the parameter n is &gt;= 0 and not
larger than the number of elements in the stack.
*/
#include <iostream>
#include <stack>
#include <queue>
using namespace std;
void display(stack<int> s)
{
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;
}

//delete this funciton after debugging
void displayq(queue<int> q)
{
    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}

//unfinished
void shift(stack<int> &s, int k)
{

    if (s.empty())
    {
        return;
    }
    queue<int> auxiliary_queue;
    int stack_size=s.size();
    while (!s.empty())
    {
        int top = s.top();
        s.pop();
        auxiliary_queue.push(top);
    }
    display(s);
    displayq(auxiliary_queue);
    for (int i = 0; i < stack_size-k; i++)
    {
        
        int front = auxiliary_queue.front();
        auxiliary_queue.pop();
        s.push(front);
    }
    display(s);
    displayq(auxiliary_queue);
}

int main()
{
    stack<int> s;
    s.push(1);
    s.push(2);
    s.push(3);
    s.push(4);
    s.push(5);
    //display(s);
    shift(s, 2);
    //display(s);
}