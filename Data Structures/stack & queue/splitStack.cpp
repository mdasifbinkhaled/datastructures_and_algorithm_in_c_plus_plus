/*
Write a function named splitStack that accepts as a parameter a
reference to a Stack of integers, and re-orders it so that all
the non-negative numbers are at the top in the reverse of their
original relative order, and all the negative numbers are at the
bottom in the reverse of their original relative order. For
example, if passed the stack {4, 0, -1, 5, -6, -3, 2, 7}, your
function should modify the stack to store {-3, -6, -1, 7, 2, 5,
0, 4}.
Constraints: Do not declare any auxiliary data structures (e.g.
arrays, Grids, Vectors, etc.) other than a single Queue of
integers.
*/
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

void splitStack(stack<int> &s)
{
    if (s.empty())
    {
        return;
    }
    queue<int> auxiliary_queue;
    
    while (!s.empty())
    {
        int top = s.top();
        s.pop();
        auxiliary_queue.push(top);
    }
    int queue_size = auxiliary_queue.size();
    for (int i = 0; i < queue_size; i++)
    {
        int front=auxiliary_queue.front();
        if(front<0){
            auxiliary_queue.pop();
            s.push(front);
            
        }else{
            auxiliary_queue.pop();
            auxiliary_queue.push(front);
        }
    }
    while (!auxiliary_queue.empty())
    {
        int front=auxiliary_queue.front();
        auxiliary_queue.pop();
        s.push(front);
    }
    
}

void display(stack<int> s)
{
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;
}

int main()
{
    stack<int> s;
    s.push(1);
    s.push(-2);
    s.push(3);
    s.push(-4);
    s.push(5);
    display(s);
    splitStack(s);
    display(s);
}
