/*
Write a function named rearrange that takes a reference to a
queue of integers as a parameter and rearranges the order of the
values so that all of the even values appear before the odd
values and that otherwise preserves the original order of the
list. For example, suppose a queue called q stores this sequence
of values:
{3, 5, 4, 17, 6, 83, 1, 84, 16, 37}
The call of rearrange(q); should rearrange the queue to store the
following sequence of values:
{4, 6, 84, 16, 3, 5, 17, 83, 1, 37}
Notice that all of the evens appear at the front of the queue
followed by the odds and that the order of the evens is the same
as in the original queue and the order of the odds is the same as
in the original queue. You may use one stack as auxiliary
storage.
*/
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

void rearrange(queue<int> &q)
{
    if (q.empty())
    {
        return;
    }
    stack<int> auxiliary_stack;
    int queue_size = q.size();
    for (int k = 0; k < 2; k++)
    {
        for (int i = 0; i < queue_size; i++)
        {
            int front = q.front();
            q.pop();
            if (front % 2 != 0)
            {
                auxiliary_stack.push(front);
            }
            else
            {
                q.push(front);
            }
        }

        while (!auxiliary_stack.empty())
        {
            int top = auxiliary_stack.top();
            auxiliary_stack.pop();
            q.push(top);
        }
    }
}

void display(queue<int> q)
{
    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}

int main()
{
    queue<int> q;
    q.push(3);
    q.push(5);
    q.push(4);
    q.push(17);
    q.push(6);
    q.push(83);
    q.push(1);
    q.push(84);
    q.push(16);
    q.push(37);

    display(q);
    rearrange(q);
    display(q);
}