/*
Write a function named isPalindrome that takes a reference to a
queue of integers as a parameter and returns true if the numbers
in the queue represent a palindrome (and false otherwise). A
sequence of numbers is considered a palindrome if it is the same
in reverse order. For example, suppose a queue called q stores
these values:
{3, 8, 17, 9, 17, 8, 3}

Then the call of isPalindrome(q); should return true because this
sequence is the same in reverse order. If the queue had instead
stored these values:
{3, 8, 17, 9, 4, 17, 8, 3}

The call on isPalindrome would instead return false because this
sequence is not the same in reverse order (the 9 and 4 in the
middle don&#39;t match). The empty queue should be considered a
palindrome. You may not make any assumptions about how many
elements are in the queue and your function must restore the
queue so that it stores the same sequence of values after the
call as it did before. You may use one stack as auxiliary
storage.
*/
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

bool isPalindrome(queue<int> q)
{
    if (q.empty())
    {
        return true;
    }
    stack<int> auxiliary_stack;
    int queue_size = q.size();
    for (int i = 0; i < queue_size; i++)
    {
        int front = q.front();
        q.pop();
        auxiliary_stack.push(front);
        q.push(front);
    }
    for (int i = 0; i < queue_size; i++)
    {
        int front = q.front();
        q.pop();
        q.push(front);
        int top = auxiliary_stack.top();
        auxiliary_stack.pop();

        if (front != top)
        {
            return false;
        }
    }
    return true;
}

void display(queue<int> q)
{
    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}
int main()
{
    queue<int> q1;
    q1.push(1);
    q1.push(2);
    q1.push(3);
    q1.push(4);
    q1.push(5);
    q1.push(6);

    display(q1);
    cout << isPalindrome(q1) << endl;
    display(q1);

    queue<int> q2;
    q2.push(1);
    q2.push(2);
    q2.push(3);
    q2.push(2);
    q2.push(1);

    display(q2);
    cout << isPalindrome(q2) << endl;
    display(q2);
}