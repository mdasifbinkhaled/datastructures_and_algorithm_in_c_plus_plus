/*
Write a function named flipHalf that reverses the order of half
of the elements of a Queue of integers, passed by reference as a
parameter to your function. Your function should reverse the
order of all the elements in odd-numbered positions (position 1,
3, 5, etc.) assuming that the first value in the queue has
position 0. For example, if the queue originally stores this
sequence of numbers when the function is called:
index: 0 1 2 3 4 5 6 7
front {1, 8, 7, 2, 9, 18, 12, 0} back
Then it should store the following values after the function
finishes executing:
index: 0 1 2 3 4 5 6 7
front {1, 0, 7, 18, 9, 2, 12, 8} back
Notice that numbers in even positions (positions 0, 2, 4, 6) have
not moved. That sub-sequence of numbers is still: (1, 7, 9, 12).
But notice that the numbers in odd positions (positions 1, 3, 5,
7) are now in reverse order relative to the original. In other
words, the original sub-sequence: (8, 2, 18, 0) - has become: (0,
18, 2, 8).
Constraints: You may use a single stack as auxiliary storage.
*/
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

void flipHalf(queue<int> &q)
{

    stack<int> auxiliary_stack;
    int queue_size = q.size();
    for (int i = 0; i < queue_size; i++)
    {
        if (i % 2 == 0)
        {
            q.push(q.front());
            q.pop();
        }
        else
        {
            auxiliary_stack.push(q.front());
            q.pop();
        }
    }

    while (!auxiliary_stack.empty())
    {
        q.push(q.front());
        q.pop();
        q.push(auxiliary_stack.top());
        auxiliary_stack.pop();
    }

    if (queue_size % 2 != 0)
    {
        q.push(q.front());
        q.pop();
    }
}

void display(queue<int> q)
{
    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}

int main()
{
    queue<int> q;
    q.push(1);
    q.push(2);
    q.push(3);
    q.push(4);
    q.push(5);
    q.push(6);

    display(q);
    flipHalf(q);
    display(q);
}