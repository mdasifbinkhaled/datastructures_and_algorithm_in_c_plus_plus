/*
Write a function named reorder that accepts as a parameter a
queue of integers that are already sorted by absolute value, and
modifies it so that the integers are sorted normally. For
example, if a queue variable named q stores the following
elements:
front {1, -2, 4, 5, -7, -9, -12, 28, -34} back
Then the call of reorder(q); should modify it to store the
following values:
front {-34, -12, -9, -7, -2, 1, 4, 5, 28} back

Constraints: You may use a single stack as auxiliary storage.
*/
#include <iostream>
#include <stack>
#include <queue>
using namespace std;

void reorder(queue<int> &q)
{
    if (q.empty())
    {
        return;
    }
    stack<int> auxiliary_stack;
    int queue_size = q.size();
    int negative_reversed = false;
    for (int k = 0; k < 2; k++)
    {
        for (int i = 0; i < queue_size; i++)
        {
            int front = q.front();

            if (front < 0)
            {
                if (!negative_reversed)
                {
                    q.pop();
                    auxiliary_stack.push(front);
                }
            }
            else
            {
                q.pop();
                q.push(front);
            }
        }
        while (!auxiliary_stack.empty())
        {
            int top = auxiliary_stack.top();
            auxiliary_stack.pop();
            q.push(top);
        }

        negative_reversed = true;
    }
}

void display(queue<int> q)
{
    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}
int main()
{
    queue<int> q;
    q.push(1);
    q.push(-2);
    q.push(3);
    q.push(-4);
    q.push(5);
    display(q);
    reorder(q);
    display(q);
}