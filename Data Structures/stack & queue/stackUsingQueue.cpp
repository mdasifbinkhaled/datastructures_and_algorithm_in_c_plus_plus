/*
Your job is to implement a Stack&lt;int&gt; using only a Queue(s). That
is, you will be responsible for writing the push and pop methods
for a Stack&lt;int&gt; but your internal data representation must be a
Queue:
void push(Queue&lt;int&gt;&amp; queue, int element)
int pop(Queue&lt;int&gt;&amp; queue)
*/
#include <iostream>
#include <queue>

using namespace std;
struct stackUsingQueue
{
    queue<int> stack;

    int length = 0;
    int isEmpty()
    {
        return length == 0 ? true : false;
    }

    int stackSize()
    {
        return length;
    }
    int front()
    {
        if (!isEmpty())
        {
            return stack.front();
        }
        return -1;
    }

   void display(queue<int> stack)
{
    while (!stack.empty())
    {
        cout << stack.front() << " ";
        stack.pop();
    }
    cout << endl;
}
    void push(int value)
    {
        stack.push(value);

        for (int i = 0; i < length; i++)
        {

            int front = stack.front();
            stack.pop();
            stack.push(front);
        }
        length++;
    }

    void pop()
    {
        if (!isEmpty())
        {
            stack.pop();
            length--;
        }
    }
};

int main()
{
    stackUsingQueue suq;
    suq.display(suq.stack);
    suq.push(1);
    suq.display(suq.stack);
    suq.push(2);
    suq.display(suq.stack);
    suq.push(3);
    suq.display(suq.stack);
    suq.push(4);
    suq.display(suq.stack);
    suq.pop();
    suq.display(suq.stack);
    suq.pop();
    suq.display(suq.stack);
    suq.pop();
    suq.display(suq.stack);
    suq.pop();
    suq.display(suq.stack);
 
}
