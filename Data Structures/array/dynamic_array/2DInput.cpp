/*
The final grades of a student for his first 3 semesters at IUB are given below:
1st semester: A B A
2nd semester: B A A A
3rd semester: A B
*/

#include <iostream>
using namespace std;

int main()
{
    int number_of_semesters, number_of_courses;

    cout << "Enter the Number of Semesters" << endl;
    cin >> number_of_semesters;

    char **semesters = new char *[number_of_semesters];

    for (int i = 0; i < number_of_semesters; i++)
    {
        cout << "Enter the number of courses taken in Semester: " << (i + 1) << endl;
        cin >> number_of_courses;
        semesters[i] = new char[number_of_courses];
        for (int j = 0; j < number_of_courses; j++)
        {
            cout << "Please Enter a Grade" << endl;
            cin >> semesters[i][j];
        }
    }

    for (int i = 0; i < number_of_semesters; i++)
    {
        for (int j = 0;  semesters[i][j]; j++)
        {
            cout<<semesters[i][j]<<" ";
        }
        cout<<endl;
        
    }


    for (int i = 0; i < number_of_semesters; i++)
    {
        delete [] semesters[i];
    }

    delete [] semesters;
    
    

    return 0;
}