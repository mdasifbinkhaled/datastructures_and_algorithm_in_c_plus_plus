/*
The array below is average daily temperature in Celsius of an imaginary city Audacity for 10 days in May.

1 19 20 12 30 11 15 25 35 13 -5

Your job is to separate the temperature into 3 different categories, and then combine them in a two dimensional array.

<=10 degree celsius	

>10 & <20 degree celsius

>=20 degree celsius

Use dynamic memory where appropriate.
*/

#include <iostream>
using namespace std;

int main()
{
    int number_of_temperatures;

    // less_than_equal_10 
    // greater_than_10_less_then_20
    // greater_than_equal_20
    int * number_of_temperatures_in_each_each_category=new int[3];

    cout << "Enter the Number of Temperatures" << endl;
    cin >> number_of_temperatures;

    double * input_temperatures = new double[number_of_temperatures];

    for (int i = 0; i < number_of_temperatures; i++)
    {
        cout << "Enter Temperature" << endl;
        cin >> input_temperatures[i];

        if (input_temperatures[i] <= 10)
        {
            number_of_temperatures_in_each_each_category[0]++;
        }
        else if (input_temperatures[i] > 10 && input_temperatures[i] < 20)
        {
            number_of_temperatures_in_each_each_category[1]++;
        }
        else
        {
            number_of_temperatures_in_each_each_category[2]++;
        }
    }

    double **temperatures = new double *[3];

    temperatures[0] = new double[number_of_temperatures_in_each_each_category[0]];
    temperatures[1] = new double[number_of_temperatures_in_each_each_category[1]];
    temperatures[2] = new double[number_of_temperatures_in_each_each_category[2]];

    int x = 0, y = 0, z = 0;

    for (int i = 0; i < number_of_temperatures; i++)
    {
        if (input_temperatures[i] <= 10)
        {
            temperatures[0][x] = input_temperatures[i];
            x++;
        }
        else if (input_temperatures[i] > 10 && input_temperatures[i] < 20)
        {
            temperatures[1][y] = input_temperatures[i];
            y++;
        }
        else
        {
            temperatures[2][z] = input_temperatures[i];
            z++;
        }
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j<number_of_temperatures_in_each_each_category[i]; j++)
        {
            cout << temperatures[i][j]<<" ";
        }
        cout << endl;
    }

    for (int i = 0; i < 3; i++)
    {
        delete[] temperatures[i];
    }
    delete[] temperatures;

    return 0;
}