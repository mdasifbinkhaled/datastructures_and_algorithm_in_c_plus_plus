/*
Implement a function which takes in two addresses from the main function. 
The first address is a pointer containing 3 odd numbers, the second address
is another pointer containing 5 even numbers. Implement the function in such
a way that after calling the function, iterating the pointer for odd number
array will print even numbers and iterating the pointer for even number array 
will give odd numbers.
*/

#include <iostream>
using namespace std;

void swapAddress(int *&a, int *&b)
{
    int *temp = a;
    a = b;
    b = temp;
}
void display(int *array, int sz)
{
    for (int i = 0; i < sz; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}
int main()
{
    int odd_size = 3, even_size = 5;
    int *odd = new int[3]{1, 3, 5};
    int *even = new int[5]{2, 4, 6, 8, 10};
    display(odd, odd_size);
    display(even, even_size);
    swapAddress(odd, even);
    display(odd, even_size);
    display(even, odd_size);
    return 0;
}