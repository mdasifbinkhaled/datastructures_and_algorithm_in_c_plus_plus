/*
Given two variables fruit and vegetable defined as following:
char *fruit = “apple”;
char *vegetable = “tomato”;

Change the address of the two variables such that when fruit is printed, it prints “tomato” and when 
vegetable is printed it prints “apple”
*/

#include <iostream>
using namespace std;

void swapAddress(char const *&a, char const *&b)
{
    char const *temp = a;
    a = b;
    b = temp;
}
void display(char const *array)
{
    for (int i = 0; array[i]; i++)
    {
        cout << array[i];
    }
    cout << endl;
}
int main()
{
    char const *fruit = "apple";
    char const *vegetable = "tomato";
    display(fruit);
    display(vegetable);
    swapAddress(fruit, vegetable);
    display(fruit);
    display(vegetable);
    return 0;
}