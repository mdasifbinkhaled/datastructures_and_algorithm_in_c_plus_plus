/*
Take a string from the user as input. Then generate the content of a 2d array. The first row of the 2d array will contain vowel and the second row will contain the consonants. The size of each of the row will depend on the number of vowels and consonants respectively.
Example:
If the input string is “Today is Tuesday”, the output will be 
	
o a i u e a

T d y s T s d y

The string will only contain ‘A’-’Z’ and ‘a’-’z’.
Vowels in English language are the following characters: ‘a’,‘e’, ‘i’, ‘o’,’ u’.The rest of the characters are consonants.
Ascii value of ‘A’ is 65, and ‘a’ is 97.
Use dynamic memory where appropriate.

*/
#include <iostream>
#include <string>
using namespace std;

int main()
{
    string input_string;

    int *number_of_vowels_or_consonants = new int[2];

    cout << "Enter a String" << endl;
    getline(cin, input_string); 
    for (int i = 0; input_string[i]; i++)
    {
        if (input_string[i] == ' ')
        {
            continue;
        }
        else if (input_string[i] == 'a' || input_string[i] == 'e' || input_string[i] == 'i' ||
                 input_string[i] == 'o' || input_string[i] == 'u' || input_string[i] == 'A' ||
                 input_string[i] == 'E' || input_string[i] == 'I' || input_string[i] == 'O' ||
                 input_string[i] == 'U')
        {
            number_of_vowels_or_consonants[0]++;
        }
        else
        {
            number_of_vowels_or_consonants[1]++;
        }
    }

    char **characters = new char *[2];

    characters[0] = new char[number_of_vowels_or_consonants[0]];
    characters[1] = new char[number_of_vowels_or_consonants[1]];

    int x = 0, y = 0;

    for (int i = 0; input_string[i]; i++)
    {
        if (input_string[i] == ' ')
        {
            continue;
        }
        else if (input_string[i] == 'a' || input_string[i] == 'e' || input_string[i] == 'i' ||
                 input_string[i] == 'o' || input_string[i] == 'u' || input_string[i] == 'A' ||
                 input_string[i] == 'E' || input_string[i] == 'I' || input_string[i] == 'O' ||
                 input_string[i] == 'U')
        {
            characters[0][x] = input_string[i];
            x++;
        }
        else
        {
            characters[1][y] = input_string[i];
            y++;
        }
    }

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < number_of_vowels_or_consonants[i]; j++)
        {
            cout << characters[i][j] << " ";
        }
        cout << endl;
    }

    for (int i = 0; i < 3; i++)
    {
        delete[] characters[i];
    }
    delete[] characters;

    return 0;
}