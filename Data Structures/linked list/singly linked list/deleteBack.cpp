/*
Write a function named deleteBack that accepts a pointer to a ListNode representing the front of a linked list. 
Your function should delete the last value (the value at the back of the list) and return the deleted value. 
If the list is empty (nullptr), your function should throw a string exception.

Constraints: Do not construct any new ListNode objects in solving this problem (though you may create as many ListNode* 
pointer variables as you like). Do not use any auxiliary data structures to solve this problem (no array, vector, stack, 
queue, string, etc). Do not modify the data field of existing nodes; change the list by changing pointers only.
*/


#include <iostream>
using namespace std;


struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

void deleteBack(Node *&head)
{
    if (head != NULL)
    {
        Node *current = head;
        Node *previous = head;
        while (current->next != NULL)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = NULL;
        }

        delete uselessNode;
    }
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);

    display(head);

    deleteBack(head);
    deleteBack(head);
    deleteBack(head);
    deleteBack(head);
    
    display(head);
    
    return 0;
}