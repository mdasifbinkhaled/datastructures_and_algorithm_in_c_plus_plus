/*
Write a function named doubleList that accepts as a parameter a reference to a pointer to a ListNode representing 
the front of a linked list. Your function should double the size of a list by appending a copy of the original 
sequence to the end of the list. For example, if a variable named front points to the front of a list containing 
the following values:
{1, 35, 28, 7}

Then the call of doubleList(front); should modify the list to store the following values:

{1, 35, 28, 7, 1, 35, 28, 7}

Constraints: Do not use any auxiliary data structures to solve this problem (no array, vector, stack, queue, 
string, etc). If the original list contains N nodes, then you should construct exactly N new nodes to be added. 
Do not modify the data field of existing nodes; change the list by changing pointers only.
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

void doubleList(Node *&head)
{
    Node *current = head;
    Node *duplicate_list = NULL;

    while (current != NULL)
    {
        addBack(duplicate_list, current->value);
        if (current->next == NULL)
        {
            break;
        }
        current = current->next;
    }
    current->next = duplicate_list;
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);

    display(head);

    doubleList(head);

    display(head);

    return 0;
}