/*
Write a function named countDuplicateStrings that accepts a pointer to a ListNodeString representing the front of a 
linked list of strings. Your function should return the number of duplicates in a sorted list, case-insensitively. 
Your code should assume that the list's elements will be in case-insensitive sorted order, so that all duplicates 
will be grouped together. For example, if a variable named front points to the front of the following sequence of 
values, the call of countDuplicateStrings(front) should return 7 because there are 2 duplicates of "apple", 1 
duplicate of "bat", 1 duplicate of "car", 2 duplicates of "dog"and 1 dupe of "fox".

{"apple", "apple", "Apple", "bat", "Bat", "car", "car", "dog", "dog", "dog", "fox", "fox"}

Constraints: Do not construct any new ListNodeString objects in solving this problem (though you may create as many 
ListNode* pointer variables as you like). Do not use any auxiliary data structures to solve this problem (no array, 
vector, stack, queue, string, etc). Your function should not modify the linked list's state; the state of the list 
should remain constant with respect to your function. You should declare the function to indicate this to the caller.
*/

#include <iostream>
using namespace std;

struct Node
{
    string value;
    Node *next;
};

string toLower(string str)
{
    string lower = "";
    for (int i = 0; str[i]; i++)
    {
        if (str[i] >= 65 && str[i] <= 90)
        {
            lower = lower + char(str[i] + 32);
        }else{
            lower = lower + str[i];
        }
        
    }
    
    return lower;
}

void addBack(Node *&head, string value)
{
    Node *newNode = new Node;
    newNode->value = toLower(value);
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

int countDuplicates(Node *&head)
{
    //bool equivalence_status=false;
    int count = 0;
    
    for (Node *current = head; current != NULL; current = current->next)
    {
        
        if (current->next != NULL && current->value == current->next->value)
        {
            count++;
        }
    }
    return count;
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, "apple");
    addBack(head, "apple");
    addBack(head, "Apple");
    addBack(head, "bat");
    addBack(head, "Bat");
    addBack(head, "car");
    addBack(head, "car");
    addBack(head, "dog");
    addBack(head, "dog");
    addBack(head, "dog");
    addBack(head, "fox");
    addBack(head, "fox");
    
    display(head);

    cout << countDuplicates(head) << endl;

    display(head);

    return 0;
}