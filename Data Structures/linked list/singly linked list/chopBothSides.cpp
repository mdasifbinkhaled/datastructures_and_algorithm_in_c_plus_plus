/*
Write a function named chopBothSides that accepts a reference to a pointer to the front of a linked list, and an 
integer parameter k, and deletes kelements from the front of the list and k elements from the back of the list. 
Suppose a variable named front points to the front of a chain storing the following values:

{10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110}

The call of chopBothSides(front, 3); would change the list to store the following elements:

{40, 50, 60, 70, 80}

If we followed this by a second call of chopBothSides(front, 1);,the list would store the following elements:

{50, 60, 70}

If the list is not large enough to delete k elements from each side, do not modify the list and throw an integer exception. 
If the list contains exactly 2kelements, it should become empty as a result of the call. If k is 0 or negative, the list 
should remain unchanged. Do not leak memory; if you delete any nodes from the list, free their associated memory.
Constraints:
    • Your code should run in no worse than O(N) time, where N is the length of the list. (You may make more than one 
    pass over the list, but the number of passes you make can't grow as k or N grow.)
    • Do not use any auxiliary data structures such as arrays, vectors, queues, strings, maps, sets, etc.
    • Do not modify the data field of any nodes; you must solve the problem by changing the links between nodes.
    • You may not construct new ListNode objects, though you may create as many ListNode* pointers as you like.
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

void deleteFront(Node *&head)
{
    if (head != NULL)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        delete uselessNode;
    }
}

void deleteBack(Node *&head)
{
    if (head != NULL)
    {
        Node *current = head;
        Node *previous = head;
        while (current->next != NULL)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = NULL;
        }

        delete uselessNode;
    }
}

void chopBothSides(Node *&head, int chop)
{

    for (int i = 0; i < chop; i++)
    {
        deleteFront(head);
        deleteBack(head);
    }
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);
    addBack(head, 5);
    addBack(head, 6);
    addBack(head, 7);
    addBack(head, 8);
    display(head);
    chopBothSides(head, 3);
    display(head);

    return 0;
}