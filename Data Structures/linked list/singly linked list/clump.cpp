/*
Write a function named clump that groups together nodes in a linked list that store the same value. 
Your code should rearrange the linked list so that all occurrences of duplicate values will occur in 
consecutive order at the site of the first occurrence of that value in the list. For example, you should 
"clump" all the 4s in the list at the site of the first 4 in the list. The node clumps should remain in 
the same relative order as in the original list.

Your function accepts two parameters: a reference to a ListNode pointer representing the front of the linked list, 
and an integer max for the maximum number of values to clump together; any additional occurrences of that same value 
must be deleted (and their memory must be freed). For example, if max is 3 but there are 5 occurrences of the value 10, 
you should keep only 3 of those 5 occurrences and delete the other 2 occurrences of 10 from the list.

Suppose a ListNode pointer variable named front points to the front of a list storing the following values:
{1, 6, 5, 2, 6, 4, 5, 3, 5, 8, 5, 2, 8, 4, 5, 6, 8, 6}             // original list

After the call of clump(front, 99); , the list should store the following elements:
{1, 6, 6, 6, 6, 5, 5, 5, 5, 5, 2, 2, 4, 4, 3, 8, 8, 8}         
// after clump(front, 99);

In the preceding call, the max value passed was very large, so no elements needed to be deleted from the list. 
If the call had instead been clump(front, 2); , the list would instead store the following elements afterward. 
Notice that the third and fourth occurrence of 6, the third through fifth occurrences of 5, and the third occurrence 
of 8 were deleted.

{1, 6, 6, 5, 5, 2, 2, 4, 4, 3, 8, 8}                           
// after clump(front, 2);

Your function should work properly for a list of any size. If the value of max passed is 0 or negative, you should throw 
an integer exception.
Note that the goal of this problem is to modify the list by modifying pointers. It might be easier to solve it in other 
ways, such as by changing nodes' datavalues or by rebuilding an entirely new list, but such tactics are forbidden.
Constraints: Do not modify the data field of any nodes; you must solve the problem by changing links between nodes and 
adding newly created nodes to the list. Do not use any auxiliary data structures such as arrays, vectors, queues, maps, 
sets, strings, etc. Do not leak memory; if you delete nodes from the list, free their associated memory. 
Your code must run in no worse than O(N2) time, where N is the length of the list.
 */

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

int nodeCount(Node *head)
{
    int c = 0;
    Node *node = head;
    while (node != NULL)
    {
        node = node->next;
        c++;
    }
    return c;
}
void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}
void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}
void addNodeAt(Node *&head, int index, Node *node)
{
    Node *newNode = node;

    if (index < 0 || index > nodeCount(head))
    {
        return;
    }
    else if (index == 0)
    {
        newNode->next = head;
        head = newNode;
    }
    else
    {
        Node *previous = head;

        for (int i = 1; i < index; i++)
        {
            previous = previous->next;
        }
        newNode->next = previous->next;
        previous->next = newNode;
    }
}
Node *deleteAt(Node *&head, int index)
{

    if (index < 0 || index >= nodeCount(head))
    {
        return NULL;
    }
    else if (index == 0)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        return uselessNode;
    }
    else
    {
        Node *current = head;
        Node *previous = head;

        for (int i = 1; i <= index; i++)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = current->next;
        }

        return uselessNode;
    }
}

Node *nodeAt(Node *head, int index, int length)
{
    if (index < 0)
    {
        return NULL;
    }
    else
    {
        int i = 0;
        Node *node = head;
        while (node != NULL)
        {
            if (i == index)
            {
                return node;
            }
            node = node->next;
            i++;
        }
    }
    return NULL;
}

void clump(Node *&head, int clump)
{
    if (clump == 0)
    {
        head = NULL;
    }
    else
    {
        int current_index = 0, forward_index = 0, total_grouped = 0;
        Node *deleted_node;
        for (Node *current = head; current != NULL; current = current->next)
        {
            forward_index = current_index + 1;
            total_grouped = 1;
            Node *forward = current->next;
            while (forward != NULL)
            {
    
                if (current->value == forward->value)
                {

                    forward = forward->next;
                    deleted_node = deleteAt(head, forward_index);
                    if (deleted_node != NULL && total_grouped < clump)
                    {
                        addNodeAt(head, current_index + 1, deleted_node);
                        current = current->next;
                        current_index++;
                        total_grouped++;
                        forward_index++;
                    }
                }
                else
                {
                    forward = forward->next;
                    forward_index++;
                }
            }
            current_index++;
        }
    }
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 6);
    addBack(head, 5);
    addBack(head, 2);
    addBack(head, 6);
    addBack(head, 4);
    addBack(head, 5);
    addBack(head, 3);
    addBack(head, 5);
    addBack(head, 8);
    addBack(head, 5);
    addBack(head, 2);
    addBack(head, 8);
    addBack(head, 4);
    addBack(head, 5);
    addBack(head, 6);
    addBack(head, 8);
    addBack(head, 6);
    display(head);
    clump(head, 3);
    display(head);
}