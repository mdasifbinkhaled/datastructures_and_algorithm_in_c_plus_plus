#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

int nodeCount(Node *head)
{
    int c = 0;
    Node *node = head;
    while (node != NULL)
    {
        node = node->next;
        c++;
    }
    return c;
}

void addFront(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        newNode->next = head;
        head = newNode;
    }
}

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

void addAt(Node *&head, int index, int value)
{
    Node *newNode = new Node;
    newNode->value = value;

    if (index < 0 || index > nodeCount(head))
    {
        return;
    }
    else if (index == 0)
    {
        newNode->next = head;
        head = newNode;
    }
    else
    {
        Node *previous = head;

        for (int i = 1; i < index; i++)
        {
            previous = previous->next;
        }
        newNode->next = previous->next;
        previous->next = newNode;
    }
}

void deleteFront(Node *&head)
{
    if (head != NULL)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        delete uselessNode;
    }
}
void deleteBack(Node *&head)
{
    if (head != NULL)
    {
        Node *current = head;
        Node *previous = head;
        while (current->next != NULL)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = NULL;
        }

        delete uselessNode;
    }
}

void deleteAt(Node *&head, int index)
{
    if (index < 0 || index >= nodeCount(head))
    {
        return;
    }
    else if (index == 0)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        delete uselessNode;
    }
    else
    {
        Node *current = head;
        Node *previous = head;

        for (int i = 1; i <= index; i++)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = current->next;
        }

        delete uselessNode;
    }
}

Node *nodeAt(Node *head, int index)
{
    if (index < 0)
    {
        return NULL;
    }
    else
    {
        int i = 0;
        Node *node = head;
        while (node != NULL)
        {
            if (i == index)
            {
                return node;
            }
            node = node->next;
            i++;
        }
    }
    return NULL;
}

int indexOf(Node *&head, int value)
{
    if (head == NULL)
    {
        return -1;
    }
    else
    {
        Node *node = head;
        int c = 0;

        while (node != NULL)
        {
            if (node->value == value)
            {
                return c;
            }
            node = node->next;
            c++;
        }
    }
    return -1;
}
Node *copyList(Node *head)
{
    Node *copyHead = NULL;
    for (Node *current = head; current != NULL; current = current->next)
    {
        Node *newNode = new Node();
        newNode->value = current->value;
        newNode->next = NULL;
        if (copyHead == NULL)
        {
            copyHead = newNode;
        }
        else
        {
            Node *node = copyHead;
            while (node->next != NULL)
            {
                node = node->next;
            }
            node->next = newNode;
        }
    }
    return copyHead;
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);
    addBack(head, 15);
    display(head);
    deleteBack(head);
    display(head);
    deleteBack(head);
    display(head);
    Node *copyHead = copyList(head);
    deleteBack(copyHead);
    display(head);
    display(copyHead);
    cout << nodeAt(head, 1)->value << endl;
    return 0;
}
