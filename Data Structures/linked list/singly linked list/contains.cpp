/*
Write a function named contains that accepts a pointer to a ListNode representing the front of a linked list, 
along with an integer value. Your function should return true if the given value occurs in the list and false 
if not. If the list is empty (null), return false.

Constraints: Do not construct any new ListNode objects in solving this problem (though you may create as many 
ListNode* pointer variables as you like). Do not use any auxiliary data structures to solve this problem 
(no array, vector, stack, queue, string, etc). Your function should not modify the linked list's state; the 
state of the list should remain constant with respect to your function.
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

bool contains(Node *&head, int value)
{
    Node *current = head;
    while (current != NULL)
    {
        if (current->value == value)
        {
            return true;
        }
        current = current->next;
    }
    return false;
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    cout<< contains(head, 5) <<endl;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);
    addBack(head, 5);
    addBack(head, 6);
    addBack(head, 7);
    addBack(head, 8);
    display(head);
    cout<< contains(head, 5) <<endl;
    cout<< contains(head, 15) <<endl;
    display(head);

    return 0;
}