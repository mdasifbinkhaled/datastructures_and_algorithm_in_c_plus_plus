/*
Write a function named braid that accepts as a parameter a reference to a pointer to a ListNode representing the front of 
a linked list. Your function should interleave the reverse of the list into the original, with an element from the reversed 
list appearing after each element of the original list. For example, if a variable named front points to the front of a 
chain containing {10, 20, 30, 40}, then after a call of braid(front);,it should store {10, 40, 20, 30, 30, 20, 40, 10}.

Constraints: Do not modify the data field of existing nodes; change the list by changing pointers only. 
You can create new ListNode objects to represent the reversed list nodes, but you should not throw out the existing 
ListNodes of the original list; you should reuse them and interleave them with the new nodes you create. 
Do not use any auxiliary data structures to solve this problem (no array, vector, stack, queue, string, etc).
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

int nodeCount(Node *head)
{
    int c = 0;
    Node *node = head;
    while (node != NULL)
    {
        node = node->next;
        c++;
    }
    return c;
}

void addAt(Node *&head, int index, int value, int length)
{
    Node *newNode = new Node;
    newNode->value = value;
    if (index < 0 || index > length)
    {
        return;
    }
    else if (index == 0)
    {
        newNode->next = head;
        head = newNode;
    }
    else
    {
        Node *previous = head;

        for (int i = 1; i < index; i++)
        {
            previous = previous->next;
        }
        newNode->next = previous->next;
        previous->next = newNode;
    }
}


//incomplete

void braid(Node *head)
{
    int value, initial_linked_list_size = nodeCount(head);
    Node *current = head;
    for (int i = initial_linked_list_size; i > 0; i --)
    {
        value = current->value;
        current = current->next;
        insertValueAt(head, i, value, nodeCount(head));
    }
}


void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    insertValueAt(head, 0, 40, nodeCount(head));
    insertValueAt(head, 0, 30, nodeCount(head));
    insertValueAt(head, 0, 20, nodeCount(head));
    insertValueAt(head, 0, 10, nodeCount(head));
    display(head);
    braid(head);
    display(head);

    return 0;
}