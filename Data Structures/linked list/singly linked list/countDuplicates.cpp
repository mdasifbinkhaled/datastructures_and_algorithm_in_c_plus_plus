/*
Write a function named countDuplicates that accepts a pointer to a ListNode representing the front of a linked list. 
Your function should return the number of duplicates in a sorted list. Your code should assume that the list's elements 
will be in sorted order, so that all duplicates will be grouped together. For example, if a variable named front points 
to the front of the following sequence of values, the call of countDuplicates(front) should return 7 because there are 2 
duplicates of 1, 1 duplicate of 3, 1 duplicate of 15, 2 duplicates of 23 and 1 duplicate of 40:

{1, 1, 1, 3, 3, 6, 9, 15, 15, 23, 23, 23, 40, 40}

Constraints: Do not construct any new ListNode objects in solving this problem (though you may create as many ListNode* 
pointer variables as you like). Do not use any auxiliary data structures to solve this problem (no array, vector, stack, 
queue, string, etc). Your function should not modify the linked list's state; the state of the list should remain constant 
with respect to your function. You should declare the function to indicate this to the caller.
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

int countDuplicates(Node *&head)
{
    //bool equivalence_status=false;
    int count = 0;
    for (Node *current = head; current != NULL; current = current->next)
    {
        if (current->next != NULL && current->value == current->next->value)
        {
            count++;
        }
    }
    return count;
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 1);
    addBack(head, 1);
    addBack(head, 3);
    addBack(head, 3);
    addBack(head, 6);
    addBack(head, 9);
    addBack(head, 15);
    addBack(head, 15);
    addBack(head, 23);
    addBack(head, 23);
    addBack(head, 23);
    addBack(head, 40);
    addBack(head, 40);
    display(head);

    cout << countDuplicates(head) << endl;

    display(head);

    return 0;
}