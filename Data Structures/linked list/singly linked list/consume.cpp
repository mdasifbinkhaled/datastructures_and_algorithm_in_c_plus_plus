/*
Write a function named consume that accepts two references to pointers to the front of linked lists. 
After the call is done, all the elements from the second list should be appended to the end of the 
first one in the same order, and the second list should be empty (null). Note that either linked list 
could be initially empty. For example, if we start with the following lists, we will get the results 
shown below:

lists

list1: {1, 3, 5, 7}
list2: {2, 4, 6}

after consume(list1, list2);

list1: {1, 3, 5, 7, 2, 4, 6}
list2: {}

after consume(list2, list1);
list1: {}
list2: {2, 4, 6, 1, 3, 5, 7}

Constraints: Do not construct any new ListNode objects in solving this problem (though you may create as many ListNode* 
pointer variables as you like). Do not use any auxiliary data structures to solve this problem (no array, vector, stack, 
queue, string, etc).
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}
void addNodeBack(Node *&head, Node *node)
{
    Node *newNode = node;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

Node *deleteFront(Node *&head)
{
    if (head != NULL)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        return uselessNode;
    }
    return NULL;
}
int nodeCount(Node *head)
{
    int c = 0;
    Node *node = head;
    while (node != NULL)
    {
        node = node->next;
        c++;
    }
    return c;
}

void consume(Node *&list1, Node *&list2)
{
    Node *deleted_node;
    while (list2 != NULL)
    {
        deleted_node = deleteFront(list2);
        addNodeBack(list1, deleted_node);
    }
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *list1 = NULL;
    Node *list2 = NULL;
    addBack(list1, 1);
    addBack(list1, 2);
    addBack(list1, 3);
    addBack(list1, 4);
    addBack(list2, 5);
    addBack(list2, 6);
    addBack(list2, 7);
    addBack(list2, 8);
    display(list1);
    display(list2);
    consume(list1, list2);
    display(list1);
    display(list2);

    return 0;
}