/**
Write a function named addBack that accepts a reference to a pointer to a ListNode representing the front of a linked list, 
along with an integer value. Your function should insert the given value into a new node at the end of the list. 
For example, suppose a variable named front points to the front of a list containing the following sequence of values:

{8, 23, 19, 7, 102}

The call of addBack(front, 42); should change the list to store the following:
{8, 23, 19, 7, 102, 42}

The other values in the list should retain the same order as in the original list.
Constraints: Do not modify the data field of existing nodes; change the list by changing pointers only. 
Do not use any auxiliary data structures to solve this problem (no array, vector, stack, queue, string, etc).
*/

#include <iostream>
using namespace std;


struct Node
{
    int value;
    Node *next;
};

void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    addBack(head, 1);
    addBack(head, 2);
    addBack(head, 3);
    addBack(head, 4);
    
    display(head);
    
    return 0;
}