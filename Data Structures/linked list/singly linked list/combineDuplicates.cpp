/*
Write a function named combineDuplicates that manipulates a list of ListNode structures. The function accepts as a 
parameter a reference to a pointer to the front of a list, and modifies the list by merging any consecutive neighboring 
nodes that contain the same element value into a single node whose value is the sum of the merged neighbors. 
For example, suppose a pointer named list points to the front of a list containing the following values. 
The diagram shows the result of a call of combineDuplicates(list); on the list. The underlined areas represent the 
neighboring duplicate elements that are merged in the final result.

{3, 3, 2, 4, 4, 4, -1, -1, 4, 12, 12, 12, 12, 48, -5, -5}       
list

{3, 3, 2, 4, 4, 4, -1, -1, 4, 12, 12, 12, 12, 48, -5, -5}       

combineDuplicates(list);

{6, 2, 12, -2, 4, 48, 48, -10}                                  

result

If the list is empty or contains no duplicates, it should be unchanged by a call to your function.
Constraints: It is okay to modify the data field of existing nodes, if you like. Do not construct any new 
ListNode objects in solving this problem (though you may create as many ListNode* pointer variables as you like). 
Do not use any auxiliary data structures to solve this problem (no array, vector, stack, queue, string, etc). 
Your code must run in no worse than O(N) time, where N is the length of the list. Your code must solve the problem 
by making only a single traversal over the list, not multiple passes.
*/

#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node *next;
};

int nodeCount(Node *head)
{
    int c = 0;
    Node *node = head;
    while (node != NULL)
    {
        node = node->next;
        c++;
    }
    return c;
}
void display(Node *head)
{
    Node *node = head;
    while (node != NULL)
    {
        cout << node->value << " ";
        node = node->next;
    }
    cout << endl;
}
void addBack(Node *&head, int value)
{
    Node *newNode = new Node;
    newNode->value = value;
    newNode->next = NULL;

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *previous = head;
        while (previous->next != NULL)
        {
            previous = previous->next;
        }
        previous->next = newNode;
    }
}

Node *deleteAt(Node *&head, int index)
{

    if (index < 0 || index >= nodeCount(head))
    {
        return NULL;
    }
    else if (index == 0)
    {
        Node *uselessNode = head;
        head = head->next;
        uselessNode->next = NULL;
        return uselessNode;
    }
    else
    {
        Node *current = head;
        Node *previous = head;

        for (int i = 1; i <= index; i++)
        {
            previous = current;
            current = current->next;
        }

        Node *uselessNode = current;

        if (current == head)
        {
            uselessNode = head;
            head = NULL;
        }
        else
        {
            previous->next = current->next;
        }

        return uselessNode;
    }
}

void combineDuplicates(Node *&head)
{
    int current_index = 0, forward_index = 0;
    double current_value;
    Node *deleted_node, *starting_node;

    for (Node *current = head; current != NULL; current = current->next)
    {
        forward_index = current_index + 1;
        Node *forward = current->next;
        current_value = current->value;
        starting_node = current;
        while (forward != NULL)
        {
            if (current_value == forward->value)
            {
                forward = forward->next;
                deleted_node = deleteAt(head, forward_index);
                starting_node->value = starting_node->value + deleted_node->value;
            }
            else
            {
                break;
            }
        }
        current_index++;
    }
}

int main()
{
    Node *head = NULL;
    addBack(head, 3);
    addBack(head, 3);
    addBack(head, 2);
    addBack(head, 4);
    addBack(head, 4);
    addBack(head, 4);
    addBack(head, -1);
    addBack(head, -1);
    addBack(head, 4);
    addBack(head, 12);
    addBack(head, 12);
    addBack(head, 12);
    addBack(head, 12);
    addBack(head, 48);
    addBack(head, -5);
    addBack(head, -5);
    display(head);
    combineDuplicates(head);
    display(head);
}
