#include <iostream>
using namespace std;

struct node
{
    int value;
    node *previous = NULL;
    node *next = NULL;
};

struct DoublyLinkedList
{
    node *head = NULL;
    node *tail = NULL;
    int length = 0;

    int getLength()
    {
        int c = 0;
        for (node *current = head; current != NULL; current = current->next)
        {
            c++;
        }
        return c;
    }

    void insertBeginning(int value)
    {
        node *newNode = new node();
        newNode->value = value;

        if (head == NULL)
        {
            head = newNode;
            tail = newNode;
        }
        else
        {
            head->previous = newNode;
            newNode->next = head;
            head = newNode;
        }
    }
    void insertEnd(int value)
    {
        node *newNode = new node();
        newNode->value = value;

        if (head == NULL)
        {
            head = newNode;
            tail = newNode;
        }
        else
        {
            tail->next = newNode;
            newNode->previous = tail;
            tail = tail->next;
        }
    }

    void insertAt(int index, int value)
    {

        if (index <= getLength())
        {
            node *newNode = new node();
            newNode->value = value;

            if (index == 0)
            {
                newNode->next = head;
                head = newNode;
                if (head->next != NULL)
                {
                    node *next = head->next;
                    next->previous = head;
                }
                else
                {
                    tail = head;
                }
            }
            else
            {
                node *previous = head;
                for (int i = 1; i < index; i++)
                {
                    previous = previous->next;
                }
                newNode->next = previous->next;
                previous->next = newNode;
                newNode->previous = previous;
                if (newNode->next == NULL)
                {
                    tail = newNode;
                }
                else
                {
                    node *next = newNode->next;
                    next->previous = newNode;
                }
            }
        }
    }
    void deleteBeginning()
    {
        if (getLength() > 0)
        {
            node *useless = head;
            head = head->next;
            if (head != NULL)
            {
                head->previous = NULL;
            }
            else
            {
                tail = NULL;
            }
            delete useless;
        }
    }
    void deleteEnd()
    {
        if (getLength() > 0)
        {
            node *useless = tail;
            tail = tail->previous;
            if (tail != NULL)
            {
                tail->next = NULL;
            }
            else
            {
                head = NULL;
            }
            delete useless;
        }
    }
    void deleteAt(int index)
    {
        if (index < getLength())
        {
            if (index == 0)
            {
                node *useless = head;
                head = head->next;
                if (head != NULL)
                {
                    head->previous = NULL;
                }
                else
                {
                    tail = NULL;
                }
                delete useless;
            }
            else
            {
                node *useless = head;
                for (int i = 1; i <= index; i++)
                {
                    useless = useless->next;
                }
                node *previous = useless->previous;
                previous->next = useless->next;
                if (previous->next != NULL)
                {
                    node *next = useless->next;
                    next->previous = previous;
                }
                else
                {
                    tail = previous;
                }
                delete useless;
            }
        }
    }

    void printForward()
    {
        for (node *current = head; current != NULL; current = current->next)
        {
            cout << current->value << " ";
        }
        cout << endl;
    }
    void printReverse()
    {
        for (node *current = tail; current != NULL; current = current->previous)
        {
            cout << current->value << " ";
        }
        cout << endl;
    }
};

int main()
{
    DoublyLinkedList *dll = new DoublyLinkedList();
    cout << "Printing" << endl;
    cout << "Forward" << endl;
    dll->printForward();
    cout << "Reverse" << endl;
    dll->printReverse();
    for (int i = 0; i < 5; i++)
    {
        dll->insertAt(i, i);
    }
    cout << "Printing" << endl;
    cout << "Forward" << endl;
    dll->printForward();
    cout << "Reverse" << endl;
    dll->printReverse();
    dll->insertAt(5, 10);
    cout << "Printing" << endl;
    cout << "Forward" << endl;
    dll->printForward();
    cout << "Reverse" << endl;
    dll->printReverse();
    dll->deleteAt(2);
    cout << "Printing" << endl;
    cout << "Forward" << endl;
    dll->printForward();
    cout << "Reverse" << endl;
    dll->printReverse();
    for (int i = 4; i >= 0; i--)
    {
        dll->deleteEnd();
        dll->printForward();
    }
    return 0;
}