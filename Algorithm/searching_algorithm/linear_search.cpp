#include <iostream>
using namespace std;


int linear_search(double * arr, int n, int search_key)
{
    for(int i=0; i<n; i++)
    {
        if(search_key==arr[i])
        {
            return i;
        }
    }
    return -1;
}

void display_array(double * arr, int n)
{

    for(int i=0; i<n; i++)
    {
        cout<<arr[i];
        if(i<n-1)
        {
            cout<<", ";
        }
    }
    cout<<endl;
}

int main()
{

    int n = 11;

    double * arr=new double[n] {32,154,4,654,-64,7654,-31,0,3213,75,5};

    cout<<"Input Array of Size "<<n<<endl;

    display_array(arr, n);

    int search_key=0;

    int result_index=linear_search(arr,n,search_key);

    if(result_index==-1)
    {
        cout<<search_key<<" is Not Found"<<endl;
    }
    else
    {
        cout<<search_key<<" is Found in Index "<<result_index<<endl;
    }

    return 0;
}
