#include <iostream>

using namespace std;

void merge(double * arr, int l, int m, int r)
{
    int n1 = m - l + 1;
    int n2 = r - m;

    double * L=new double[n1];
    double * R=new double[n2];

    for (int i = 0; i < n1; i++)
    {
        L[i] = arr[l + i];
    }

    for (int i = 0; i < n2; i++)
    {
        R[i] = arr[m + 1 + i];
    }
    int i = 0, j=0;

    int k = l;

    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void merge_sort(double * arr,int l,int r)
{
    if(l<r)
    {
        int m =l+ (r-l)/2;
        merge_sort(arr,l,m);
        merge_sort(arr,m+1,r);
        merge(arr,l,m,r);
    }

}

int jump_search(double * arr, int n,int step, int search_key)
{


    int previous=0;

    int current_step;

    for(current_step=step-1; arr[min(n,current_step-1)]<search_key; current_step+=step)
    {

        //starting of previous block
        previous = current_step;

        if(previous>=n)
        {
            return -1;
        }
    }

    for( ; arr[previous]<search_key; previous++)
    {
        if(previous == min(current_step,n))
        {
            return -1;
        }
    }

    if(arr[previous]==search_key)
    {
        return previous;
    }


    return -1;
}

void display_array(double * arr, int n)
{

    for(int i=0; i<n; i++)
    {
        cout<<arr[i];
        if(i<n-1)
        {
            cout<<", ";
        }
    }
    cout<<endl;
}


int main()
{

    int n = 11;

    double * arr=new double[n] {32,154,4,654,-64,7654,-31,0,3213,75,5};

    cout<<"Input Array of Size "<<n<<endl;

    display_array(arr, n);

    merge_sort(arr,0,n-1);

    cout<<"Input Array After Sorting"<<endl;

    display_array(arr, n);

    int search_key=7654;

    int step = 4; // best if it is sqrt(n)

    int result_index=jump_search(arr,n,step,search_key);

    if(result_index==-1)
    {
        cout<<search_key<<" is Not Found"<<endl;
    }
    else
    {
        cout<<search_key<<" is Found in Index "<<result_index<<endl;
    }

    return 0;
}
