#include <iostream>

using namespace std;

void merge(double * arr, int l, int m, int r)
{
    int n1 = m - l + 1;
    int n2 = r - m;

    double * L=new double[n1];
    double * R=new double[n2];

    for (int i = 0; i < n1; i++)
    {
        L[i] = arr[l + i];
    }

    for (int i = 0; i < n2; i++)
    {
        R[i] = arr[m + 1 + i];
    }
    int i = 0, j=0;

    int k = l;

    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void merge_sort(double * arr,int l,int r)
{
    if(l<r)
    {
        int m =l+ (r-l)/2;
        merge_sort(arr,l,m);
        merge_sort(arr,m+1,r);
        merge(arr,l,m,r);
    }

}

int binary_search(double * arr, int l, int r,int search_key)
{
    if(l<=r)
    {
        int mid = (l + r) / 2;

        if (arr[mid] == search_key)
        {
            return mid;
        }
        if (arr[mid] > search_key)
        {
            return binary_search(arr, l, mid - 1, search_key);
        }
        else
        {
            return binary_search(arr, mid + 1, r, search_key);
        }
    }
    return -1;
}



int exponential_search(double * arr, int n,int search_key)
{

    if (n == 0)
    {
        return -1;
    }

    int bound = 1;

    while (bound < n && arr[bound] < search_key)
    {
        bound = bound * 2;
    }

    return binary_search(arr, bound/2, min(bound + 1, n), search_key);

}


void display_array(double * arr, int n)
{

    for(int i=0; i<n; i++)
    {
        cout<<arr[i];
        if(i<n-1)
        {
            cout<<", ";
        }
    }
    cout<<endl;
}


int main()
{

    int n = 11;

    double * arr=new double[n] {32,154,4,654,-64,7654,-31,0,3213,75,5};

    cout<<"Input Array of Size "<<n<<endl;

    display_array(arr, n);

    merge_sort(arr,0,n-1);

    cout<<"Input Array After Sorting"<<endl;

    display_array(arr, n);

    int search_key=4;

    int result_index=exponential_search(arr,n,search_key);

    if(result_index==-1)
    {
        cout<<search_key<<" is Not Found"<<endl;
    }
    else
    {
        cout<<search_key<<" is Found in Index "<<result_index<<endl;
    }

    return 0;
}

