#include <iostream>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

struct Node
{
    int cost=-1;
    int id;
    int parent;
    bool status;
    vector <pair<int,Node *>> v;
};

struct comparator
{
    bool operator()(Node const *  n1,Node const *  n2)
    {
        return n1->cost>n2->cost;

    }
};

void addEdge(vector <Node *> & v,int source, int destination,int cost)
{
    v[source]->v.push_back(make_pair(cost,v[destination]));
}

void display(vector <Node *> v)
{
    cout<<"(Source Vertex, Vertex Cost, Vertex Parent)->(Destination Vertex, Vertex Cost)"<<endl;
    for(int i=0; i<v.size(); i++)
    {
        cout<<"("<<i<<", "<<v[i]->cost<<", "<<v[i]->parent<<")";
        for(int j=0; j<v[i]->v.size(); j++)
        {
            cout<<" -> ("<<v[i]->v[j].second->id<<", "<<v[i]->v[j].first<<")";
        }
        if(v[i]->v.size()==0)
        {
            cout<<" -> NULL";
        }
        cout<<endl;
    }
}
void updatePriorityQueue(priority_queue <Node *,vector<Node*>,comparator> & pq)
{
    priority_queue <Node *,vector<Node*>,comparator> update_pq (pq);

    while(!pq.empty())
    {
        pq.pop();
    }
    while(!update_pq.empty())
    {
        pq.push(update_pq.top());
        update_pq.pop();
    }


}
void primsMST(vector <Node *> &v)
{
    for(int i=0; i<v.size(); i++)
    {
        v[i]->cost=INT_MAX;
        v[i]->parent=-1;
        v[i]->status=false;
    }
    v[0]->cost=0;
    priority_queue <Node *,vector<Node*>,comparator> pq;
    for(int i=0; i<v.size(); i++)
    {
        pq.push(v[i]);
    }
    while(!pq.empty())
    {
        updatePriorityQueue(pq);
        Node * min_node=pq.top();
        pq.pop();
        min_node->status=true;
        for(int i=0; i<min_node->v.size(); i++)
        {
            if(min_node->v[i].second->status==false && min_node->v[i].first<min_node->v[i].second->cost)
            {
                min_node->v[i].second->cost=min_node->v[i].first;
                min_node->v[i].second->parent=min_node->id;

            }
        }

    }

}

int main()
{
    int V=5;
    int E=7;

    vector <Node*> v;
    for(int i=0; i<V; i++)
    {
        v.push_back(new Node);
        v[i]->id=i;
    }
    display(v);
    addEdge(v,0,1,6);
    addEdge(v,0,2,1);
    addEdge(v,2,4,1);
    addEdge(v,2,3,1);
    addEdge(v,3,1,2);
    addEdge(v,4,0,3);
    addEdge(v,4,3,2);
    display(v);
    primsMST(v);
    display(v);
    return 0;
}
