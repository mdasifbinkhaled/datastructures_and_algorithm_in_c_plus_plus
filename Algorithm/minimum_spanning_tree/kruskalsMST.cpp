#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;


struct Edge
{
    int id;
    int weight;
    int source;
    int destination;
};
struct edgeSet
{
    int parent;
    int rank_value;
};
struct Graph
{
    int V;
    int E;
    vector <Edge *> edge;
};

void addEdge(vector <Edge *> & edge,int id,int source, int destination,int weight)
{
    Edge *node=new Edge;
    node->id=id;
    node->source=source;
    node->destination=destination;
    node->weight=weight;
    edge.push_back(node);

}

bool compare(Edge * e1,Edge * e2)
{
    return e1->weight<e2->weight;
}

int findSet(edgeSet * edge_set, int current_set)
{
    if (edge_set[current_set].parent!=current_set)
    {
        edge_set[current_set].parent=findSet(edge_set,edge_set[current_set].parent);
    }
    return edge_set[current_set].parent;
}

void setUnion(edgeSet * edge_set,int source_set,int destination_set)
{

    if(edge_set[source_set].rank_value<edge_set[destination_set].rank_value)
    {
        edge_set[source_set].parent=destination_set;
        edge_set[destination_set].rank_value++;
    }
    else if(edge_set[source_set].rank_value>edge_set[destination_set].rank_value)
    {
        edge_set[destination_set].parent=source_set;
        edge_set[source_set].rank_value++;
    }
    else
    {
        edge_set[source_set].parent=destination_set;
        edge_set[destination_set].rank_value++;
    }
}

void displayHierarchy(edgeSet * edge_set,Graph * graph )
{

    for(int i=0; i<graph->V; i++)
    {
        cout<<i;
        int store=edge_set[i].parent;
        while(store!=edge_set[store].parent)
        {
            cout<<"-->"<<store;
            store=edge_set[store].parent;
        }
        cout<<endl;
    }
}

void displayMST(Edge ** selected_edges,int number_of_selected_edges){

    cout<<"(Source Vertex)->(Destination Vertex, Vertex Cost)"<<endl;
    for(int i=0; i<number_of_selected_edges; i++)
    {
        cout<<"("<<selected_edges[i]->source<<") -> ("<<selected_edges[i]->destination<<", "<<selected_edges[i]->weight<<")"<<endl;
    }
}

void kruskalsMST(Graph * graph)
{

    sort(graph->edge.begin(),graph->edge.end(),compare);

    Edge ** selected_edges=new Edge*[graph->V-1];

    edgeSet *edge_set=new edgeSet[graph->V];

    for(int i=0; i<graph->V; i++)
    {
        edge_set[i].parent=i;
        edge_set[i].rank_value=0;
    }

    int number_of_selected_edges=0;

    for(int i=0; (i<graph->E)&&(number_of_selected_edges<(graph->V-1)); i++)
    {
        //displayHierarchy(edge_set,graph);
        int source_set=findSet(edge_set,graph->edge[i]->source);
        int destination_set=findSet(edge_set,graph->edge[i]->destination);

        if(source_set!=destination_set)
        {
            selected_edges[number_of_selected_edges++] = graph->edge[i];
            setUnion(edge_set,source_set,destination_set);
        }
    }

    displayMST(selected_edges,number_of_selected_edges);

}

int main()
{
    int V=5;
    int E=7;

    Graph * graph=new Graph;
    graph->V=V;
    graph->E=E;


    //display(v);
    addEdge(graph->edge,1,0,1,6);
    addEdge(graph->edge,2,0,2,1);
    addEdge(graph->edge,3,2,4,1);
    addEdge(graph->edge,4,2,3,1);
    addEdge(graph->edge,5,3,1,2);
    addEdge(graph->edge,6,4,0,3);
    addEdge(graph->edge,7,4,3,2);
    //display(v);
    //kruskalsMST(edge,V,E);
    //display(v);
    kruskalsMST(graph);
    return 0;
}

