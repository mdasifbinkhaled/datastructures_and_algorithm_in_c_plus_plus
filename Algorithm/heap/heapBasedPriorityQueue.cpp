#include <iostream>
#include <cmath>
using namespace std;

int capacity = 10;
int heap_size = 0;
int *heap = new int[capacity];

int getParentIndex(int index)
{
    return index / 2;
}
int getLeftChildrenIndex(int index)
{
    return 2 * index + 1;
}
int getRightChildrenIndex(int index)
{
    return 2 * index + 2;
}
void heapIncreaseKey(int index, int key)
{
    if (key < heap[index])
    {
        cout << "New Key Is Smaller Than The Current Key." << endl;
    }
    else
    {
        heap[index] = key;
        while (index > 0 && heap[getParentIndex(index)] < heap[index])
        {
            swap(heap[index], heap[getParentIndex(index)]);
            index = getParentIndex(index);
        }
    }
}
void maxHeapInsert(int key)
{
    heap_size++;
    heap[heap_size - 1] = INT32_MIN;
    heapIncreaseKey(heap_size - 1, key);
}
void maxHeapify(int index)
{
    int left = getLeftChildrenIndex(index);
    int right = getRightChildrenIndex(index);
    int largest;
    if (left < heap_size && heap[left] > heap[index])
    {
        largest = left;
    }
    else
    {
        largest = index;
    }

    if (right < heap_size && heap[right] > heap[largest])
    {
        largest = right;
    }

    if (largest != index)
    {
        swap(heap[largest], heap[index]);
        maxHeapify(largest);
    }
}
void buildMaxHeap()
{
    for (int i = ceil(heap_size / 2) - 1; i >= 0; i--)
    {
        maxHeapify(i);
    }
}

int heapExtractMax()
{
    if (heap_size < 1)
    {
        cout << "Heap Underflow" << endl;
    }
    int maximum = heap[0];
    heap[0] = heap[heap_size - 1];
    heap_size--;
    maxHeapify(0);
    return maximum;
}

void displayHeap()
{
    for (int i = 0; i < heap_size; i++)
    {
        cout << heap[i] << " ";
    }
    cout << endl;
}
void displayArray()
{
    for (int i = 0; i < capacity; i++)
    {
        cout << heap[i] << " ";
    }
    cout << endl;
}

int main()
{
    maxHeapInsert(5);
    maxHeapInsert(1);
    maxHeapInsert(2);
    maxHeapInsert(4);
    maxHeapInsert(3);
    maxHeapInsert(6);
    displayHeap();
    displayArray();
    maxHeapInsert(7);
    maxHeapInsert(1);
    displayHeap();
    displayArray();
    for (int i = 0; i < 10; i++)
    {
        cout<<i<<"th Iteration"<<endl;
        cout << heapExtractMax() << endl;
        displayHeap();
        displayArray();
    }

    return 0;
}