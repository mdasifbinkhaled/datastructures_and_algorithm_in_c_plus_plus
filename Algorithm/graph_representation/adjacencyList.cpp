#include <iostream>
#include <vector>

using namespace std;

struct Node
{
    int cost;
    int id;
    vector <pair<int,Node *>> v;
};

void addEdge(vector <Node *> v,int source, int destination,int cost)
{
    v[source]->v.push_back(make_pair(cost,v[destination]));
}

void display(vector <Node *> v){
    cout<<"Source Vertex->(Destination Vertex, Vertex Cost)"<<endl;
    for(int i=0;i<v.size();i++){
        cout<<i;
        for(int j=0;j<v[i]->v.size();j++){
            cout<<" -> ("<<v[i]->v[j].second->id<<", "<<v[i]->v[j].first<<")";
        }
        if(v[i]->v.size()==0){
            cout<<" -> NULL";
        }
        cout<<endl;
    }
}

int main()
{
    int V=4;
    int E=5;

    vector <Node*> v;
    for(int i=0; i<V; i++)
    {
        v.push_back(new Node);
        v[i]->id=i;
    }
    display(v);
    addEdge(v,0,1,1);
    addEdge(v,0,2,3);
    addEdge(v,0,3,2);
    addEdge(v,1,2,5);
    addEdge(v,2,3,6);
    display(v);
    return 0;
}
