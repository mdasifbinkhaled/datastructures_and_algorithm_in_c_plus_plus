#include <iostream>
using namespace std;

int main()
{
    int a=2;
    int b=3;

    int maximum = (a > b) ? a : b;

    while(true){

        if(maximum%a==0 && maximum% b==0){
            cout<<"LCM of "<<a<<" and "<<b<<" is "<<maximum<<endl;
            break;
        }
        maximum++;
    }

    return 0;
}
