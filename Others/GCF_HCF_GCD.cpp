#include <iostream>
using namespace std;

int main()
{
    int a=12;
    int b=16;
    int gcf_hcf_gcd;

    int minimum=(a>b)?b:a;

    for(int i=minimum; i>=1; i--)
    {

        if(a%i==0 && b%i==0)
        {
            gcf_hcf_gcd = i;
            break;
        }
    }

    cout<<"GCF/HCF/GCD of "<<a<<" and "<<b<<" is "<<gcf_hcf_gcd<<endl;

    return 0;
}
